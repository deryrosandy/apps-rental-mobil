-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 28 Agu 2020 pada 19.01
-- Versi server: 10.1.35-MariaDB
-- Versi PHP: 7.2.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `apps_restrukturisasi_db`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `nasabah`
--

CREATE TABLE `nasabah` (
  `nasabah_id` int(11) NOT NULL,
  `nama_lengkap` varchar(100) DEFAULT NULL,
  `noktp` varchar(20) NOT NULL,
  `norek` varchar(20) DEFAULT NULL,
  `notelp` int(15) DEFAULT NULL,
  `alamat` text,
  `jenis_kelamin` enum('L','P') DEFAULT NULL,
  `tgl_lahir` date DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1',
  `email` varchar(50) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Dumping data untuk tabel `nasabah`
--

INSERT INTO `nasabah` (`nasabah_id`, `nama_lengkap`, `noktp`, `norek`, `notelp`, `alamat`, `jenis_kelamin`, `tgl_lahir`, `status`, `email`, `created_at`, `user_id`) VALUES
(8, 'kiting', '1123456789160992', '719701001727506', 2147483647, 'priuk', 'L', '1992-09-16', 1, 'vikikedutan@gmail.com', '2020-08-28 18:24:53', 19);

-- --------------------------------------------------------

--
-- Struktur dari tabel `pengajuan`
--

CREATE TABLE `pengajuan` (
  `pengajuan_id` int(11) NOT NULL,
  `nasabah_id` int(11) DEFAULT NULL,
  `pinjaman_id` int(11) DEFAULT NULL,
  `nama_nasabah` varchar(50) DEFAULT NULL,
  `no_telp` varchar(15) DEFAULT NULL,
  `no_rek` varchar(20) DEFAULT NULL,
  `jml_pinjaman` decimal(20,0) DEFAULT NULL,
  `tenor` int(11) DEFAULT NULL,
  `lama_penangguhan` int(11) DEFAULT NULL,
  `alamat` text,
  `keterangan` text,
  `status` enum('open','approved','reject','closed') DEFAULT 'open',
  `created_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Dumping data untuk tabel `pengajuan`
--

INSERT INTO `pengajuan` (`pengajuan_id`, `nasabah_id`, `pinjaman_id`, `nama_nasabah`, `no_telp`, `no_rek`, `jml_pinjaman`, `tenor`, `lama_penangguhan`, `alamat`, `keterangan`, `status`, `created_at`, `updated_by`, `updated_at`) VALUES
(7, 8, 7, 'kiting', '2147483647', '719701001727506', '50000000', 12, 6, 'priuk', 'seret', 'approved', '2020-08-28 18:28:11', 1, '2020-08-28 18:30:29');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pinjaman`
--

CREATE TABLE `pinjaman` (
  `pinjaman_id` int(11) NOT NULL,
  `nasabah_id` int(11) DEFAULT NULL,
  `tenor` int(11) DEFAULT NULL,
  `jml_pinjaman` decimal(20,0) DEFAULT NULL,
  `cicilan_ke` int(11) DEFAULT NULL,
  `tgl_pinjaman` date DEFAULT NULL,
  `tgl_cicilan_terakhir` date DEFAULT NULL,
  `cicilan_perbulan` varchar(20) DEFAULT NULL,
  `jenis_pinjaman` enum('kur','komersil') DEFAULT 'kur',
  `suku_bunga` int(11) DEFAULT NULL,
  `status` enum('berjalan','lunas','nunggak') DEFAULT 'berjalan',
  `created_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Dumping data untuk tabel `pinjaman`
--

INSERT INTO `pinjaman` (`pinjaman_id`, `nasabah_id`, `tenor`, `jml_pinjaman`, `cicilan_ke`, `tgl_pinjaman`, `tgl_cicilan_terakhir`, `cicilan_perbulan`, `jenis_pinjaman`, `suku_bunga`, `status`, `created_at`) VALUES
(7, 8, 12, '50000000', 5, '2020-03-25', '2021-03-25', '4400000', '', 100000, 'berjalan', '2020-08-28 18:26:13');

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `user_id` int(11) NOT NULL,
  `username` varchar(25) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  `nama_lengkap` varchar(50) DEFAULT NULL,
  `email` varchar(30) DEFAULT NULL,
  `status` tinyint(11) DEFAULT '1',
  `created_at` datetime DEFAULT NULL,
  `user_type` enum('admin','nasabah') DEFAULT 'admin'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`user_id`, `username`, `password`, `nama_lengkap`, `email`, `status`, `created_at`, `user_type`) VALUES
(1, 'admin', 'e10adc3949ba59abbe56e057f20f883e', 'Administrator BRI', 'admin@admin.com', 1, '2020-08-04 05:20:33', 'admin'),
(19, 'kiting', 'd0970714757783e6cf17b26fb8e2298f', 'kiting', 'fikifiki@gmail.com', 1, '2020-08-28 18:27:17', 'nasabah');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `nasabah`
--
ALTER TABLE `nasabah`
  ADD PRIMARY KEY (`nasabah_id`) USING BTREE;

--
-- Indeks untuk tabel `pengajuan`
--
ALTER TABLE `pengajuan`
  ADD PRIMARY KEY (`pengajuan_id`) USING BTREE;

--
-- Indeks untuk tabel `pinjaman`
--
ALTER TABLE `pinjaman`
  ADD PRIMARY KEY (`pinjaman_id`) USING BTREE;

--
-- Indeks untuk tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`) USING BTREE;

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `nasabah`
--
ALTER TABLE `nasabah`
  MODIFY `nasabah_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT untuk tabel `pengajuan`
--
ALTER TABLE `pengajuan`
  MODIFY `pengajuan_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT untuk tabel `pinjaman`
--
ALTER TABLE `pinjaman`
  MODIFY `pinjaman_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT untuk tabel `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
