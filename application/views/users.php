<div class="msg" style="">
  <?php echo $this->session->flashdata('msg'); ?>
</div>

  <div class="row">
    <div class="col-md-11">
          <a style="margin-bottom: 15px;" id="tambah_user" class="pull-right btn btn-md btn-primary"><i class="fa fa-plus"></i> Tambah User</a>
    </div>
  </div>

<div class="row">
  <div class="col-md-11">
  
    <div class="nav-tabs-custom">
      <ul class="nav nav-tabs">
        <li class="active"><a href="#users" data-toggle="tab">Users</a></li>
        <li><a href="#settings" data-toggle="tab">Settings</a></li>
      </ul>
      
      <div class="tab-content">
        <div class="active tab-pane" id="users">
      
         <div class="form-horizontal table-stripe ">
            <table id="data_users" class="tabel-report table table-striped">
              <thead>
                <tr>
                  <th>No.</th>
                  <th>Username</th>
                  <th>Nama Karyawan</th>
                  <th>Role</th>
                  <th>Status</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody id="list_data_user">

              </tbody>
            </table>
          </div>

        </div>
        <div class="tab-pane" id="settings">
          <form class="form-horizontal" action="<?php echo base_url('profile/change_password') ?>" method="POST">
            <div class="form-group">
              <label for="passLama" class="col-sm-3 control-label">Password Lama</label>
              <div class="col-sm-9">
                <input type="password" class="form-control" placeholder="Password Lama" name="passLama">
              </div>
            </div>
            <div class="form-group">
              <label for="passBaru" class="col-sm-3 control-label">Password Baru</label>
              <div class="col-sm-9">
                <input type="password" class="form-control" placeholder="Password Baru" name="passBaru">
              </div>
            </div>
            <div class="form-group">
              <label for="passKonf" class="col-sm-3 control-label">Konfirmasi Password</label>
              <div class="col-sm-9">
                <input type="password" class="form-control" placeholder="Konfirmasi Password" name="passKonf">
              </div>
            </div>
            
            <div class="form-group">
              <div class="col-sm-offset-3 col-sm-9">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>

  </div>
</div>


<div id="addusersModal" class="modal fade">
	<div class="modal-dialog">
		<form action="<?php echo base_url(); ?>master/add_user" class="form-horizontal" id="adduser_form" method="post" enctype="multipart/form-data">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Edit User</h4>
				</div>
				<div class="modal-body">
          <div class="form-group">
						<label class="control-label col-xs-3" for="kodetrp">Username :</label>
						<div class="col-xs-9">
							<input type="text" name="username"  class="form-control"  placeholder="Username" required>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-xs-3" for="namakar">Password :</label>
						<div class="col-xs-9">
							<input type="text"  name="password" class="form-control" placeholder="Password">
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-xs-3" for="phoneNumber">Nama Lengkap :</label>
						<div class="col-xs-9">
							<input type="text" class="form-control" name="fullname" placeholder="Nama Lengkap">
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-xs-3" for="level">Role :</label>
						<div class="col-md-6 col-xs-12">
							<select name="level" class="form-control" >
                  <option value="1">HRD</option>
                  <option value="2">Manager</option>
                  <option value="3">Karyawan</option>
							</select>
						</div>
					</div>
          <div class="form-group">
            <label class="control-label col-xs-3" for="branch_id">Status :</label>
            <div class="col-md-6 col-xs-12">
              <label>
                <input type="checkbox" name="status" class="minimal" checked />
              </label>
            </div>							
          </div>							
				</div>
				<div class="modal-footer">
					<input type="hidden" name="operation" />
					<input type="submit" id="" class="btn btn-success" value="Submit" />
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</div>
		</form>
	</div>
</div>

<div id="usersModal" class="modal fade">
	<div class="modal-dialog">
		<form action="<?php echo base_url(); ?>master/update_user" class="form-horizontal" id="user_form" method="post" enctype="multipart/form-data">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Edit User</h4>
				</div>
				<div class="modal-body">
          <div class="form-group">
						<label class="control-label col-xs-3" for="kodetrp">Username :</label>
						<div class="col-xs-9">
							<input type="text" name="username"  class="form-control" id="username" placeholder="Username" required>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-xs-3" for="namakar">Password :</label>
						<div class="col-xs-9">
							<input type="text"  name="password" class="form-control" id="password" placeholder="Biarkan Kosong Jika Password Tidak Di Rubah">
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-xs-3" for="phoneNumber">Nama Lengkap :</label>
						<div class="col-xs-9">
							<input type="text" class="form-control" name="fullname" id="fullname" placeholder="Nama Lengkap">
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-xs-3" for="level">Level User :</label>
						<div class="col-md-6 col-xs-12">
							<select name="level" class="form-control" id="level">
									<option value="1">HRD</option>
                  <option value="2">Manager</option>
                  <option value="3">Karyawan</option>
							</select>
						</div>
					</div>
          <div class="form-group">
            <label class="control-label col-xs-3" for="branch_id">Status :</label>
            <div class="col-md-6 col-xs-12">
              <label id="checked_status">
                
              </label>
            </div>							
          </div>								
				</div>
				<div class="modal-footer">
					<input type="hidden" name="user_id" id="user_id" class="user_id" />
					<input type="hidden" name="operation" id="operation" />
					<input type="submit" id="action" class="btn btn-success" value="Submit" />
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</div>
		</form>
	</div>
</div>