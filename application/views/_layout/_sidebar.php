<aside class="main-sidebar">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">
	
	 <!-- Sidebar user panel (optional) -->
    <div class="user-panel">
      <div class="pull-left image">
        <img src="<?php echo base_url(); ?>assets/img/profil1.jpg" class="img-circle" alt="User Image">
      </div>
      <div class="pull-left info">
        <p><?php echo $userdata->nama_lengkap; ?></p>
        <!-- Status -->
        <a href="<?php echo base_url(); ?>assets/#"><i class="fa fa-circle text-success"></i> Online</a>
      </div>
    </div>

    <!-- Sidebar Menu -->
    <ul class="sidebar-menu">
		<li class="header">MAIN MENU</li>
		<!-- Optionally, you can add icons to the links -->

		<li <?php if ($page == 'dashboard') {echo 'class="active"';} ?>>
			<a href="<?php echo base_url('dashboard'); ?>">
			<i class="fa fa-dashboard"></i>
			<span>Dashboard</span>
			</a>
		</li>
		
		<?php if($this->userdata->user_type == 'admin') { ?> <!-- For Admin -->
			<?php /*
			<li <?php if ($page == 'pengajuan') {echo 'class="active"';} ?>>
				<a href="<?php echo base_url('restrukturisasi/tambah_data'); ?>">
					<i class="fa fa-pencil-square-o"></i>
					<span>Input Restrukturisasi</span>
				</a>
			</li>
			*/ ?>
			<li class="treeview <?php if ($page == 'data_open' || $page == 'data_proses' || $page == 'data_ditolak' || $page == 'data_restrukturisasi') {echo 'active';} ?>"><a href="<?php echo site_url()?>#"> <i class="fa fa-envelope"></i>
				<span>Data Restrukturisasi</span> <i class="fa fa-angle-left pull-right"></i></a>
				<ul class="treeview-menu">
					<li class="<?php echo ($page == 'data_open' ? 'active': ''); ?>">
						<a href="<?php echo site_url()?>restrukturisasi/data_open"><i class="fa fa-caret-right"></i></i>
							Restrukturisasi Masuk
						</a>
					</li>
					<li class="<?php echo ($page == 'data_proses' ? 'active': ''); ?>">
						<a href="<?php echo site_url()?>restrukturisasi/data_approved"><i class="fa fa-caret-right"></i></i>
							Restrukturisasi Di Setujui
						</a>
					</li>
					<li class="<?php echo ($page == 'data_ditolak' ? 'active': ''); ?>">
						<a href="<?php echo site_url()?>restrukturisasi/data_reject"><i class="fa fa-caret-right"></i></i>
							Restrukturisasi Di Tolak
						</a>
					</li>
					<li class="<?php echo ($page == 'data_restrukturisasi' ? 'active': ''); ?>">
						<a href="<?php echo site_url()?>restrukturisasi/data_restrukturisasi"><i class="fa fa-caret-right"></i></i>
							Semua Data
						</a>
					</li>
				</ul>
			</li>

			<li class="treeview <?php if ($page == 'data_nasabah' || $page == 'tambah_nasabah') {echo 'active';} ?>"><a href="<?php echo site_url()?>#"> <i class="fa fa-users"></i>
				<span>Data Nasabah</span> <i class="fa fa-angle-left pull-right"></i></a>
				<ul class="treeview-menu">
					<li class="<?php echo ($page == 'tambah_nasabah' ? 'active' : ''); ?>">
						<a href="<?php echo site_url()?>nasabah/tambah_nasabah"><i class="fa fa-caret-right"></i></i>
							Tambah Data
						</a>
					</li>
					<li class="<?php echo ($page == 'data_nasabah' ? 'active' : ''); ?>">
						<a href="<?php echo site_url()?>nasabah/index"><i class="fa fa-caret-right"></i></i>
							Data Nasabah
						</a>
					</li>
				</ul>
			</li>

			<li class="treeview <?php echo (($page == 'laporan_pengajuan' || $page == 'laporan_nasabah' || $page == 'laporan_pinjaman') ? 'active' : ''); ?>"><a href="<?php echo site_url()?>#"> <i class="fa fa-print"></i>
				<span>Laporan</span> <i class="fa fa-angle-left pull-right"></i></a>
				<ul class="treeview-menu">
					<li class="<?php echo ($page == 'laporan_pengajuan' ? 'active' : ''); ?>">
						<a href="<?php echo site_url()?>laporan/data_restrukturisasi"><i class="fa fa-caret-right"></i></i>
							Data Restrukturisasi
						</a>
					</li>
					<li class="<?php echo ($page == 'laporan_nasabah' ? 'active' : ''); ?>">
						<a href="<?php echo site_url()?>laporan/data_nasabah"><i class="fa fa-caret-right"></i></i>
							Data Nasabah
						</a>
					</li>
					<li class="<?php echo ($page == 'laporan_pinjaman' ? 'active' : ''); ?>">
						<a href="<?php echo site_url()?>laporan/data_pinjaman"><i class="fa fa-caret-right"></i></i>
							Data Pinjaman
						</a>
					</li>
				</ul>
			</li>
			
			<li class="treeview <?php if ($page == 'data_pinjaman' || $page == 'data_users') {echo 'active';} ?>"><a href="<?php echo site_url()?>master/"> <i class="fa fa-gears"></i>
				<span>Master</span> <i class="fa fa-angle-left pull-right"></i></a>
				<ul class="treeview-menu">
					<li class="<?php echo ($page == 'data_users' ? 'active' : ''); ?>">
						<a href="<?php echo site_url()?>master/data_users"><i class="fa fa-caret-right"></i></i>
							User
						</a>
					</li>
					<li class="<?php echo ($page == 'data_pinjaman' ? 'active' : ''); ?>">
						<a href="<?php echo site_url()?>master/data_pinjaman"><i class="fa fa-caret-right"></i></i>
							Data Pinjaman
						</a>
					</li>
				</ul>
			</li>
		<?php } ?>

		<?php if($this->userdata->user_type == 'nasabah') { ?> <!-- For Nasabah -->

			<li <?php if ($page == 'pengajuan') {echo 'class="active"';} ?>>
				<a href="<?php echo base_url('restrukturisasi/create_new'); ?>">
					<i class="fa fa-pencil-square-o"></i>
					<span>Input Restrukturisasi</span>
				</a>
			</li>

			<li <?php if ($page == 'data_restrukturisasi') {echo 'class="active"';} ?>>
				<a href="<?php echo base_url('restrukturisasi/my_data'); ?>">
					<i class="fa fa-envelope"></i>
					<span>Data Restrukturisasi</span>
				</a>
			</li>

		<?php } ?>

		<li <?php if ($page == 'ganti_password') {echo 'class="active"';} ?>>
			<a href="<?php echo base_url('ganti_password'); ?>">
				<i class="fa fa-lock"></i>
				<span>Ganti Password</span>
			</a>
		</li>

    </ul>
    <!-- /.sidebar-menu -->
  </section>
  <!-- /.sidebar -->
</aside>