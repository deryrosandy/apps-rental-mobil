<link rel="stylesheet" href="<?php echo base_url(); ?>assets/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/eksternal/font-awesome.min.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/eksternal/ionicons.min.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/AdminLTE.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/skins/skin-blue.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/select2/select2.min.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/iCheck/all.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/datatables/dataTables.bootstrap.css">
<!-- bootstrap daterangepicker -->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/daterangepicker/daterangepicker.css">
<!-- bootstrap datepicker -->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/datepicker/css/bootstrap-datepicker.min.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/morris.js/morris.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/rwd-table.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/sweetalert.css">
<link href="<?php echo base_url(); ?>assets/plugins/datetimepicker/css/bootstrap.min.css" rel="stylesheet" media="screen">
<link href="<?php echo base_url(); ?>assets/plugins/datetimepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet" media="screen">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/style.css">
