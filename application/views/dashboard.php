<div class="row  gridlist-dashboard">

	<?php if ($this->userdata->user_type == 'admin') { ?>

		<div class="col-lg-3 col-sm-4 col-xs-6">
			<div class="small-box bg-aqua">
				<div class="inner">
					<h3><?php echo $total_pengajuan; ?></h3>
					<p>Total Restrukturisasi</p>
				</div>
				<div class="icon">
					<i class="fa fa-calendar-o"></i>
				</div>
				<a href="<?php echo base_url('restrukturisasi/data_restrukturisasi'); ?>" class="small-box-footer">Buka Data <i class="fa fa-arrow-circle-right"></i></a>
			</div>
		</div>

		<div class="col-lg-3 col-sm-4 col-xs-6">
			<div class="small-box bg-blue">
				<div class="inner">
					<h3><?php echo $total_cicilan; ?></h3>
					<p>Total Cicilan</p>
				</div>
				<div class="icon">
					<i class="fa fa-book"></i>
				</div>
				<a href="<?php echo base_url('master/data_pinjaman'); ?>" class="small-box-footer">Buka Data <i class="fa fa-arrow-circle-right"></i></a>
			</div>
		</div>

		<div class="col-lg-3 col-sm-4 col-xs-6">
			<div class="small-box bg-light-blue">
				<div class="inner">
					<h3><?php echo $total_nasabah; ?></h3>
					<p>Total Nasabah</p>
				</div>
				<div class="icon">
					<i class="fa fa-medkit"></i>
				</div>
				<a href="<?php echo base_url('nasabah/index'); ?>" class="small-box-footer">Buka Data <i class="fa fa-arrow-circle-right"></i></a>
			</div>
		</div>

		<div class="col-lg-3 col-sm-4 col-xs-6">
			<div class="small-box bg-olive">
				<div class="inner">
					<h3><?php echo $total_user; ?></h3>
					<p>Total User</p>
				</div>
				<div class="icon">
					<i class="fa fa-car"></i>
				</div>
				<a href="<?php echo base_url('master/data_users'); ?>" class="small-box-footer">Buka Data <i class="fa fa-arrow-circle-right"></i></a>
			</div>
		</div>

	<?php } ?>

	<?php if ($this->userdata->user_type == 'nasabah') { ?>

		<div class="col-md-10">
			<div class="box box-primary">
				<div class="box-header with-border">
					<h3 class="box-title">Informasi Prosedur Proses Restrukturisasi Pinjaman</h3>
				</div>
				<!-- /.box-header -->
				<div class="row">
					<div class="col-md-12 col-md-offset-0">
						<div class="col-md-12 col-md-offset-0">

							<div class="box-body">
								<div class="form-group">
									<div class="col-sm-12">
										<div class="alert alert-info info-dismissible">
											<h4><i class="icon fa fa-info-circle"></i> Proses permintaan restrukturisasi pinjaman maksimal 7 hari.</h4>
											<h4><i class="icon fa fa-info-circle"></i> 1 Pinjaman hanya bisa melakukan permintaan 1 (satu) kali.</h4>
											<h4><i class="icon fa fa-info-circle"></i> Silahkan melakukan permintaan restrukturisasi di form yang telah disediakan.</h4>
										</div>
									</div>
								</div>
							</div>

						</div>
					</div>
				</div>
			</div>
		</div>

	<?php } ?>

</div>