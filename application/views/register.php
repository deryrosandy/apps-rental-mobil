<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Registrasi Pengajuan Restrukturisasi Cicilan BRI</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/AdminLTE.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/iCheck/square/blue.css">
  <script>
    function isNumber(e) {
        e = e || window.event;
        var charCode = e.which ? e.which : e.keyCode;
        return /\d/.test(String.fromCharCode(charCode));
      }
  </script>

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>

<body class="hold-transition register-page">
  <div class="register-box" style="width: 500px;">
    <div class="register-logo">
      <h3><b>Sistem Informasi Restrukturisasi Cicilan</b></h3>
    </div>

    <div class="register-box-body">
      <p class="login-box-msg">Registrasi User Nasabah</p>

      <form action="<?php echo base_url(); ?>register/submit_register" method="post">
        <div class="form-group has-feedback">
          <input type="text" onkeypress="return isNumber(event);" class="form-control" id="no_ktp" name="no_ktp" placeholder="Masukkan NIK KTP" required>
        </div>
        <div class="form-group has-feedback">
          <div class="input-group input-group-sm">
            <input name="no_rek" onkeypress="return isNumber(event);" id="no_rek" placeholder="Masukkan No. Rekening" type="text" required class="form-control">
            <span class="input-group-btn">
              <button type="button" id="cek_nasabah" class="btn btn-primary btn-flat">Cek Data Nasabah</button>
            </span>
          </div>
        </div>
        <input type="hidden" class="form-control" id="nasabah_id" name="nasabah_id">
        <div class="form-group has-feedback">
          <input type="text" class="form-control" required name="username" placeholder="Username" required>
        </div>
        <div class="form-group has-feedback">
          <input type="email" class="form-control" name="email" placeholder="Email">
        </div>
        <div class="form-group has-feedback">
          <input type="password" class="form-control" required name="password" placeholder="Password">
        </div>
        <div class="row">
          <!-- /.col -->
          <div class="col-lg-offset-8 col-xs-4">
            <button type="submit" class="btn btn-primary btn-block btn-flat">Registrasi</button>
          </div>
          <!-- /.col -->
        </div>
      </form>
    </div>
    <!-- /.form-box -->
  </div>
  <!-- /.register-box -->

  <!-- jQuery 2.2.3 -->
  <script src="<?php echo base_url(); ?>assets/plugins/jQuery/jquery-2.2.3.min.js"></script>
  <!-- Bootstrap 3.3.6 -->
  <script src="<?php echo base_url(); ?>assets/bootstrap/js/bootstrap.min.js"></script>
  <!-- iCheck -->
  <script src="<?php echo base_url(); ?>assets/plugins/iCheck/icheck.min.js"></script>
  <script>
    $(document).ready(function() {

      $(document).on("click", "#cek_nasabah", function() {

        var no_ktp = $("#no_ktp").val();
        var no_rek = $("#no_rek").val();
        
        $.ajax({
          type: "POST",
          url: "<?php echo site_url('register/cek_nasabah'); ?>",
          data: {
            "no_ktp": no_ktp,
            "no_rek": no_rek,
          },
          dataType: "json",
          success: function(response) {
            if(response==false){
              alert("Data Nasabah Tidak Di Temukan");
              location.reload();
            } else {
              $("#nasabah_id").val(response.nasabah_id);
              alert("Data Nasabah Di Temukan");
            }
              
          },
          error: function() {

            alert("Gagal");

          }
        });

      });

      $("#modal_edit_komisi_kuesioner button[type=submit]").click(function() {
        $.ajax({
          type: "POST",
          url: "<?php echo site_url('report/update_komisi_kuesioner'); ?>",
          data: $('form.form_komisi_kuesioner').serialize(),
          success: function(msg) {
            $("#status").html(msg)
            $("#modal_edit_komisi_kuesioner").modal('hide');
            location.reload();
          },
          error: function() {
            alert("Gagal");
          }
        });
      });


    });

    $(function() {
      $('input').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue',
        increaseArea: '20%' // optional
      });
    });
  </script>
</body>

</html>