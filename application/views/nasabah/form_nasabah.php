<div class="msg" style="display:none;">
	<?php echo @$this->session->flashdata('msg'); ?>
</div>

<div class="row">
	<div class="col-md-8">
		<div class="box box-primary">
			<div class="box-header with-border">
				<h3 class="box-title">Form Nasabah</h3>
			</div>
			<!-- /.box-header -->
			<div class="row">
				<div class="col-md-11">
					<!-- form start -->
					<form action="<?php echo base_url(); ?>nasabah/insert_nasabah" class="form-horizontal"  onsubmit="return validasi_input(this)" id="" method="post" enctype="multipart/form-data">
						<div class="box-body">

							<div class="form-group">
								<label for="nama_lengkap" class="col-sm-4 control-label">Nama Lengkap</label>

								<div class="col-sm-8">
									<input type='text' value="<?php echo (isset($nasabah) ? $nasabah->nama_lengkap : ''); ?>" class="form-control" placeholder="Nama Lengkap" name="nama_lengkap" required />
								</div>
							</div>

							<div class="form-group">
								<label for="noktp" class="col-sm-4 control-label">No. KTP</label>

								<div class="col-sm-8">
									<input type='text' value="<?php echo (isset($nasabah) ? $nasabah->noktp : ''); ?>" class="form-control" minlength="16" placeholder="Masukkan 16 Digit NIK KTP" name="noktp" required />
								</div>
							</div>

							<div class="form-group">
								<label for="notelp" class="col-sm-4 control-label">No. Telp</label>

								<div class="col-sm-8">
									<input type='text' value="<?php echo (isset($nasabah) ? $nasabah->notelp : ''); ?>" minlength="11" placeholder="Masukkan No. Telp" class="form-control" name="notelp" required />
								</div>
							</div>

							<div class="form-group">
								<label for="jenis_kelamin" class="col-sm-4 control-label">Jenis Kelamin</label>

								<div class="col-sm-8">
									<div class="radio">
										<label>
											<input type="radio" name="jenis_kelamin" required value="L" <?php echo ((@$nasabah->jenis_kelamin == "L") ? "checked" : ''); ?>>
											Laki-laki
										</label>
									</div>
									<div class="radio">
										<label>
											<input type="radio" name="jenis_kelamin" required value="P" <?php echo ((@$nasabah->jenis_kelamin == "P") ? "checked" : ''); ?>>
											Perempuan
										</label>
									</div>
								</div>

							</div>

							<div class="form-group">
								<label for="norek" class="col-sm-4 control-label">No. Rekening</label>

								<div class="col-sm-8">
									<input type='text' value="<?php echo (isset($nasabah) ? $nasabah->norek : ''); ?>"  minlength="15" placeholder="Masukkan 15 Digit No. Rekening" class="form-control" name="norek" required />
								</div>
							</div>

							<div class="form-group">
								<label for="tgl_nasabah" class="col-sm-4 control-label">Tanggal Lahir</label>

								<div class="col-sm-8">
									<div class="input-group date datetimepicker" data-date-format="yyyy-mm-dd">
										<input type='text' value="<?php echo (isset($nasabah) ? $nasabah->tgl_lahir : ''); ?>" class="form-control" placeholder="Masukkan Tanggal Lahir"  name="tgl_lahir" />
										<span class="input-group-addon">
											<span class="glyphicon glyphicon-calendar"></span>
										</span>
									</div>
								</div>

							</div>

							<div class="form-group">
								<label for="email" class="col-sm-4 control-label">Email</label>

								<div class="col-sm-8">
									<input type='email' value="<?php echo (isset($nasabah) ? $nasabah->email : ''); ?>" class="form-control" placeholder="Email" name="email" />
								</div>
							</div>

							<div class="form-group">
								<label class="control-label col-xs-4" for="jenis_cuti">Status Nasabah</label>
								<div class="col-sm-8">
									<div class="radio">
										<label>
											<input type="radio" name="status" id="optionsRadios1" required value="1" <?php echo ((@$nasabah->status == "1") ? "checked" : ''); ?>>
											Aktif
										</label>
									</div>
									<div class="radio">
										<label>
											<input type="radio" name="status" id="optionsRadios2" required value="0" value="kur" <?php echo ((@$nasabah->status == "0") ? "checked" : ''); ?>>
											Tidak Aktif
										</label>
									</div>
								</div>
							</div>

							<div class="form-group">
								<label for="alamat" class="col-sm-4 control-label">Alamat</label>

								<div class="col-sm-8">
									<textarea class="form-control" row="4" name="alamat" placeholder="Masukkan Alamat" ></textarea>
								</div>
							</div>

							<input class="form-control" type="hidden" name="nasabah_id" value="<?php echo (isset($nasabah) ? $nasabah->nasabah_id : ''); ?>">

						</div>
						<!-- /.box-body -->
						<div class="box-footer">
							<button type="submit" value="flag" name="flag" class="margin-5 btn-group btn btn-primary pull-right">Simpan</button>
							<button type="submit" onclick="history.back(-1)" class="margin-5 btn-group btn btn-default pull-right">Batal</button>
						</div>
						<!-- /.box-footer -->
					</form>
				</div>
			</div>
		</div>
	</div>
</div>