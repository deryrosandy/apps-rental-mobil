<div class="msg">
	<?php echo @$this->session->flashdata('msg'); ?>
</div>

<div class="row">
	<div class="col-md-12">
		<a href="<?php echo base_url(); ?>nasabah/tambah_nasabah" style="margin-bottom: 15px;" class="pull-right btn btn-md btn-primary"><i class="fa fa-plus"></i> Tambah Nasabah</a>
	</div>
</div>

<div class="row">
	<div class="col-md-12 col-lg-12">
		<div class="box">
			<!-- /.box-header -->
			<div class="box-body">
				<div class="form-horizontal table-responsive col-lg-12">
					<table id="datatable1" class="tabel-report table table-bordered table-striped  center-all">
						<thead>
							<tr>
								<th>No.</th>
								<th>Nama Nasabah</th>
								<th>No. KTP</th>
								<th>No. Telp</th>
								<th>No. Rek</th>
								<th>Jenis Kelamin</th>
								<th>Tgl Lahir</th>
								<th>Status</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
							<?php $no = 1; ?>
							<?php foreach ($nasabah as $nas) : ?>
								<tr>
									<td><?php echo $no; ?></td>
									<td><?php echo $nas->nama_lengkap; ?></td>
									<td><?php echo $nas->noktp; ?></td>
									<td><?php echo $nas->notelp; ?></td>
									<td><?php echo $nas->norek; ?></td>
									<td><?php echo get_jenis_kelamin($nas->jenis_kelamin); ?></td>
									<td><?php echo tgl_indo2($nas->tgl_lahir); ?></td>
									<td><?php echo status_active($nas->status); ?></td>
									<td>
										<div class="btn-group" style="display: -webkit-inline-box">
											<a href="<?php echo base_url(); ?>nasabah/edit_nasabah/<?php echo $nas->nasabah_id; ?>" class="btn btn-xs btn-primary" nasabah_id="<?php echo $nas->nasabah_id; ?>"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
											<button class="btn btn-xs btn-danger delete" data-id="<?php echo $nas->nasabah_id; ?>" data-url="<?php echo site_url('nasabah/delete_nasabah')?>" ><i class="fa fa-trash" aria-hidden="true"></i></button>
										</div>
									</td>

								</tr>
								<tr>
									<?php $no++; ?>
								<?php endforeach; ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>

</div>