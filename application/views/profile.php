<div class="row">

  <div class="col-md-8">
    <div class="nav-tabs-custom">
      <ul class="nav nav-tabs">
        <li><a href="#profile" data-toggle="tab">User Profile</a></li>
      </ul>
      <div class="tab-content">
        <div class="active tab-pane" id="profile">
          <form class="form-horizontal" action="<?php echo base_url('profile/update') ?>" method="POST" enctype="multipart/form-data">
            <div class="form-group">
              <label for="inputUsername" class="col-sm-3 control-label">Username</label>
              <div class="col-sm-8">
                <input type="text" class="form-control" disabled id="username" placeholder="Username" name="username" value="<?php echo $userdata->username; ?>">
              </div>
            </div>
            <div class="form-group">
              <label for="fullname" class="col-sm-3 control-label">Nama Lengkap</label>
              <div class="col-sm-8">
                <input type="text" class="form-control" placeholder="Nama Lengkap" name="nama_lengkap" value="<?php echo $userdata->nama_lengkap; ?>">
              </div>
            </div>
            <div class="form-group">
              <label for="fullname" class="col-sm-3 control-label">Email</label>
              <div class="col-sm-8">
                <input type="text" class="form-control" placeholder="Email" name="email" value="<?php echo $userdata->email; ?>">
              </div>
            </div>

            <div class="form-group">
              <div class="col-sm-offset-3 col-sm-8">
                <button type="submit" class="btn btn-primary">Simpan</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>

    <div class="msg" style="">
      <?php echo $this->session->flashdata('msg'); ?>
    </div>

  </div>
</div>