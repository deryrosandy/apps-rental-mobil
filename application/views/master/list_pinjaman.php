<div class="msg">
  <?php echo @$this->session->flashdata('msg'); ?>
</div>

<div class="row">
	<div class="col-md-12">
			<a href="<?php echo base_url(); ?>master/tambah_pinjaman" style="margin-bottom: 15px;" class="pull-right btn btn-md btn-primary"><i class="fa fa-plus"></i> Tambah Pinjaman</a>
	</div>
</div>

<div class="row">
	<div class="col-md-12 col-lg-12">
		<div class="box">
			<!-- /.box-header -->
			<div class="box-body">
				<div class="table-responsive col-lg-12">
					<table id="datatable1" class="tabel-report table table-bordered table-striped  center-all">
						<thead>
							<tr>
								<th>No.</th>
								<th>Nama Nasabah</th>
								<th>Jml Pinjaman</th>
								<th>No. Rek</th>
								<th>Tgl Pinjaman</th>								
								<th>Bunga Perbulan</th>
								<th>Cicilan Perbulan</th>								
								<th>Cicilan Ke</th>			
								<th>Jenis Pinjaman</th>					
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
							<?php $no = 1; ?>
							<?php foreach ($pinjaman as $pinj): ?>
								<tr>
									<td><?php echo $no; ?></td>																							
									<td><?php echo get_nama_nasabah($pinj->nasabah_id); ?></td>	
									<td><?php echo format_digit($pinj->jml_pinjaman); ?></td>	
									<td><?php echo $pinj->norek; ?></td>	
									<td><?php echo tgl_indo2($pinj->tgl_pinjaman); ?></td>										
									<td><?php echo format_digit($pinj->suku_bunga); ?></td>				
									<td><?php echo format_digit($pinj->cicilan_perbulan); ?></td>															
									<td><?php echo $pinj->cicilan_ke; ?></td>									
									<td><?php echo $pinj->jenis_pinjaman; ?></td>									
									<td>
										<div class="btn-group" style="display: -webkit-inline-box">
											<a href="<?php echo base_url(); ?>master/edit_pinjaman/<?php echo $pinj->pinjaman_id; ?>" class="btn btn-xs btn-primary edit_pinjaman" pinjaman_id="<?php echo $pinj->pinjaman_id; ?>"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
											<button class="btn btn-xs btn-danger delete" data-id="<?php echo $pinj->pinjaman_id; ?>" data-url="<?php echo site_url('master/delete_pinjaman')?>" ><i class="fa fa-trash" aria-hidden="true"></i></button>
										</div>
									</td>
									
									</tr>
								<tr>
								<?php $no++; ?>
							<?php endforeach; ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	
</div>