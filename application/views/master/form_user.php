<div class="msg" style="display:none;">
	<?php echo @$this->session->flashdata('msg'); ?>
</div>

<div class="row">
	<div class="col-md-12">
		<div class="box box-primary">
			<div class="box-header with-border">
				<h3 class="box-title">Form User</h3>
			</div>
			<!-- /.box-header -->
			<div class="row">
				<div class="col-md-8">
					<!-- form start -->
					<form action="<?php echo base_url(); ?>master/insert_user" class="form-horizontal" id="" method="post" enctype="multipart/form-data">
						<div class="box-body">

							<div class="form-group">
								<label for="username" class="col-sm-4 control-label">Username</label>

								<div class="col-sm-8">
									<input type='text' value="<?php echo (isset($user) ? $user->username : ''); ?>" class="form-control" name="username" required />
								</div>
							</div>

							<div class="form-group">
								<label for="nama_lengkap" class="col-sm-4 control-label">Nama Lengkap</label>

								<div class="col-sm-8">
									<input type='text' value="<?php echo (isset($user) ? $user->nama_lengkap : ''); ?>" class="form-control" name="nama_lengkap" required />
								</div>
							</div>

							<div class="form-group">
								<label for="nama_lengkap" class="col-sm-4 control-label">Password</label>

								<div class="col-sm-8">
									<input type='password' placeholder="Kosongkan jika password tidak diganti" class="form-control" name="password" />
								</div>
							</div>

							<div class="form-group">
								<label for="email" class="col-sm-4 control-label">Email</label>

								<div class="col-sm-8">
									<input type='email' value="<?php echo (isset($user) ? $user->email : ''); ?>" class="form-control" name="email" />
								</div>

							</div>

							<div class="form-group">
								<label for="jenis_user" class="col-sm-4 control-label">User Type</label>

								<div class="col-sm-8">
									<div class="radio pull-left">
										<label>
											<input type="radio" name="user_type" id="optionsRadios1" value="admin" <?php echo ((@$user->user_type == "admin") ? "checked" : ''); ?>>
											Admin
										</label>
									</div>
									<div class="radio pull-left margin">
										<label>
											<input type="radio" name="user_type" id="optionsRadios2" value="nasabah" <?php echo ((@$user->user_type == "nasabah") ? "checked" : ''); ?>>
											Nasabah
										</label>
									</div>
								</div>

							</div>

							<div class="form-group">
								<label class="control-label col-xs-4" for="jenis_cuti">Status User</label>
								<div class="col-md-8">
									<div class="radio pull-left">
										<label>
											<input type="radio" name="status" id="optionsRadios1" value="1" <?php echo ((@$user->status == "1") ? "checked" : ''); ?>>
											Aktif
										</label>
									</div>
									<div class="radio pull-left margin">
										<label>
											<input type="radio" name="status" id="optionsRadios2" value="lunas" value="0" <?php echo ((@$user->status == "0") ? "checked" : ''); ?>>
											Tidak Aktif
										</label>
									</div>
								</div>
							</div>

							<input class="form-control" type="hidden" name="user_id" value="<?php echo (isset($user) ? $user->user_id : ''); ?>">

						</div>
						<!-- /.box-body -->
						<div class="box-footer">
							<button type="submit" value="flag" name="flag" class="margin-5 btn-group btn btn-primary pull-right">Simpan</button>
							<button type="submit" onclick="history.back(-1)" class="margin-5 btn-group btn btn-default pull-right">Batal</button>
						</div>
						<!-- /.box-footer -->
					</form>
				</div>
			</div>
		</div>
	</div>
</div>