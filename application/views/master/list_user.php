<div class="msg">
  <?php echo @$this->session->flashdata('msg'); ?>
</div>

<div class="row">
	<div class="col-md-12">
			<a href="<?php echo base_url(); ?>master/tambah_user" style="margin-bottom: 15px;" class="pull-right btn btn-md btn-primary"><i class="fa fa-plus"></i> Tambah User</a>
	</div>
</div>

<div class="row">
	<div class="col-md-12 col-lg-12">
		<div class="box">
			<!-- /.box-header -->
			<div class="box-body">
				<div class="table-responsive col-lg-12">
					<table id="datatable1" class="table table-bordered table-striped dataTable">
						<thead>
							<tr>
								<th>No.</th>
								<th>Username</th>
								<th>Nama Lengkap</th>
								<th>Email</th>
								<th>User Type</th>
								<th>Status</th>								
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
							<?php $no = 1; ?>
							<?php foreach ($users as $user): ?>
								<tr>
									<td><?php echo $no; ?></td>																							
									<td><?php echo $user->username; ?></td>	
									<td><?php echo $user->nama_lengkap; ?></td>	
									<td><?php echo $user->email; ?></td>	
									<td><?php echo ucfirst($user->user_type); ?></td>										
									<td><?php echo status_active($user->status); ?></td>										
									<td>
										<div class="btn-group" style="display: -webkit-inline-box">
											<a href="<?php echo base_url(); ?>master/edit_user/<?php echo $user->user_id; ?>" class="btn btn-xs btn-primary edit_user" user_id="<?php echo $user->user_id; ?>"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
											<button class="btn btn-xs btn-danger delete"  data-id="<?php echo $user->user_id; ?>" data-url="<?php echo site_url('master/delete_user')?>" ><i class="fa fa-trash" aria-hidden="true"></i></button>
										</div>
									</td>
									
									</tr>
								<tr>
								<?php $no++; ?>
							<?php endforeach; ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	
</div>