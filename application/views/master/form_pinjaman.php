<div class="msg" style="display:none;">
	<?php echo @$this->session->flashdata('msg'); ?>
</div>

<div class="row">
	<div class="col-md-12">
		<div class="box box-primary">
			<div class="box-header with-border">
				<h3 class="box-title">Form Pinjaman</h3>
			</div>
			<!-- /.box-header -->
			<div class="row">
				<div class="col-md-8">
					<!-- form start -->
					<form action="<?php echo base_url(); ?>master/insert_pinjaman" class="form-horizontal" id="" method="post" enctype="multipart/form-data">
						<div class="box-body">

							<div class="form-group">
								<label for="dari_tanggal" class="col-sm-4 control-label">Nasabah</label>
								<div class="col-sm-8">
									<select class="form-control select2" id="user_type" name="nasabah_id" data-placeholder="- Pilih Nasabah -">
										<option label="- Pilih Nasabah -"></option>
										<?php foreach ($nasabah as $nas) { ?>
											<option <?php echo ((@$pinjaman->nasabah_id == $nas->nasabah_id) ? "selected" : ''); ?> value="<?php echo $nas->nasabah_id; ?>"><?php echo $nas->nama_lengkap; ?></option>
										<?php } ?>
									</select>
								</div>
							</div>

							<div class="form-group">
								<label for="jml_pinjaman" class="col-sm-4 control-label">Jumlah Pinjaman</label>

								<div class="col-sm-8">
									<input type='number' value="<?php echo (isset($pinjaman) ? $pinjaman->jml_pinjaman : ''); ?>" class="form-control" name="jml_pinjaman" required />
								</div>
							</div>

							<div class="form-group">
								<label class="control-label col-xs-4" for="jenis_cuti">Tenor Pinjaman</label>
								<div class="col-md-8">
									<select name="tenor" class="form-control select2" id="tenor" required>
										<option <?php echo ((@$pinjaman->tenor == "12") ? "selected" : ''); ?> value="12">12 Bulan</option>
										<option <?php echo ((@$pinjaman->tenor == "18") ? "selected" : ''); ?> value="18">18 Bulan</option>
										<option <?php echo ((@$pinjaman->tenor == "24") ? "selected" : ''); ?> value="24">24 Bulan</option>
										<option <?php echo ((@$pinjaman->tenor == "36") ? "selected" : ''); ?> value="26">26 Bulan</option>
										<option <?php echo ((@$pinjaman->tenor == "48") ? "selected" : ''); ?> value="48">48 Bulan</option>
										<option <?php echo ((@$pinjaman->tenor == "60") ? "selected" : ''); ?> value="60">60 Bulan</option>
									</select>
								</div>
							</div>

							<div class="form-group">
								<label for="tgl_pinjaman" class="col-sm-4 control-label">Awal Pinjam</label>

								<div class="col-sm-8">
									<div class="input-group date datetimepicker" data-date-format="yyyy-mm-dd">
										<input type='text' value="<?php echo (isset($pinjaman) ? $pinjaman->tgl_pinjaman : ''); ?>" class="form-control" name="tgl_pinjaman" required />
										<span class="input-group-addon">
											<span class="glyphicon glyphicon-calendar"></span>
										</span>
									</div>
								</div>

							</div>

							<div class="form-group">
								<label for="tgl_cicilan_terakhir" class="col-sm-4 control-label">Tgl Lunas Cicilan</label>

								<div class="col-sm-8">
									<div class="input-group date datetimepicker" data-date-format="yyyy-mm-dd">
										<input type='text' value="<?php echo (isset($pinjaman) ? $pinjaman->tgl_cicilan_terakhir : ''); ?>" class="form-control" name="tgl_cicilan_terakhir" required />
										<span class="input-group-addon">
											<span class="glyphicon glyphicon-calendar"></span>
										</span>
									</div>
								</div>

							</div>

							<div class="form-group">
								<label for="jml_pinjaman" class="col-sm-4 control-label">Cicilan Ke</label>

								<div class="col-sm-8">
									<input type='number' value="<?php echo (isset($pinjaman) ? $pinjaman->cicilan_ke : ''); ?>" class="form-control" name="cicilan_ke" required />
								</div>
							</div>

							<div class="form-group">
								<label for="cicilan_perbulan" class="col-sm-4 control-label">Cicilan Perbulan</label>

								<div class="col-sm-8">
									<input type='number' value="<?php echo (isset($pinjaman) ? $pinjaman->cicilan_perbulan : ''); ?>" class="form-control" name="cicilan_perbulan" required />
								</div>
							</div>

							<div class="form-group">
								<label for="jenis_pinjaman" class="col-sm-4 control-label">Jenis Pinjaman</label>

								<div class="col-sm-8">
									<div class="radio">
										<label>
											<input type="radio" name="jenis_pinjaman" id="optionsRadios1" value="kur"  <?php echo ((@$pinjaman->jenis_pinjaman == "kur") ? "checked" : ''); ?>>
											KUR
										</label>
									</div>
									<div class="radio">
										<label>
											<input type="radio" name="jenis_pinjaman" id="optionsRadios2" value="komersil"  <?php echo ((@$pinjaman->jenis_pinjaman == "komersil") ? "checked" : ''); ?>>
											Komersil
										</label>
									</div>
								</div>

							</div>

							<div class="form-group">
								<label for="suku_bunga" class="col-sm-4 control-label">Jumlah Bunga Perbulan</label>

								<div class="col-sm-8">
									<input type='number' value="<?php echo (isset($pinjaman) ? $pinjaman->suku_bunga : ''); ?>" class="form-control" name="suku_bunga" required />
								</div>
							</div>

							<div class="form-group">
								<label class="control-label col-xs-4" for="jenis_cuti">Status Pinjaman</label>
								<div class="col-md-8">
									<div class="col-sm-8">
										<div class="radio">
											<label>
												<input type="radio" name="status" id="optionsRadios1" value="berjalan" <?php echo ((@$pinjaman->status == "berjalan") ? "checked" : ''); ?>>
												Berjalan
											</label>
										</div>
										<div class="radio">
											<label>
												<input type="radio" name="status" id="optionsRadios2" value="lunas" value="kur" <?php echo ((@$pinjaman->status == "lunas") ? "checked" : ''); ?>>
												Lunas
											</label>
										</div>
									</div>
								</div>
							</div>

							<input class="form-control" type="hidden" name="pinjaman_id" value="<?php echo (isset($pinjaman) ? $pinjaman->pinjaman_id : ''); ?>">

						</div>
						<!-- /.box-body -->
						<div class="box-footer">
							<button type="submit" value="flag" name="flag" class="margin-5 btn-group btn btn-primary pull-right">Simpan</button>
							<button type="submit" onclick="history.back(-1)" class="margin-5 btn-group btn btn-default pull-right">Batal</button>
						</div>
						<!-- /.box-footer -->
					</form>
				</div>
			</div>
		</div>
	</div>
</div>