<div class="msg" style="display:none;">
  <?php echo @$this->session->flashdata('msg'); ?>
</div>

<div class="row">
	<div class="col-md-12">
		<div class="box box-primary">

            <div class="box-header with-border">
              <h3 class="box-title">Cetak Data Nasabah</h3>
            </div>

			<!-- /.box-header -->
			<div class="row">
				<div class="col-md-6 col-md-offset-1" style="margin-top: 30px;margin-bottom: 30px;">
            	<!-- form start -->
					<form action="<?php echo base_url(); ?>laporan/print_data_nasabah" class="form-horizontal" id=""  target="_blank" method="get" enctype="multipart/form-data">			
						<div class="box-body">
							
						<?php /*
							<div class="form-group">

								<div class="col-sm-3">
									<label for="" class="col-sm-12 control-label">Pilih Kelas</label>
								</div>

								<div class="col-sm-9">
									<select name="kelas_id" class="form-control select2" id="kelas_id">
										<option value="">- Semua Kelas -</option>
										<?php foreach($data_kelas as $kelas){ ?>
											<option value="<?php echo $kelas->kelas_id; ?>"><?php echo $kelas->nama_kelas; ?> [ <?php echo $kelas->kode_kelas; ?> ]</option>
										<?php } ?>
									</select>
								</div>
							</div>

							*/ ?>

							<div class="row">

								<div class="col-sm-9">
									<button type="submit" id="btn_submit_laporan" class="btn btn-primary pull-left"><i class="fa fa-print"></i>&nbsp;&nbsp;Cetak Data Nasabah</button>
									<div>&nbsp;</div>
								</div>

							</div>

						</div>
						<!-- /.box-body -->
					</form>
         		</div>
          	</div>
		</div>
	</div>
</div>