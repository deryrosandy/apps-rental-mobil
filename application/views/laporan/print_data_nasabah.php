<style type="text/css">
	html { margin: 40px 30px 30px}
	.tg  {
		width: 100%;
		border-collapse:collapse;
		border-spacing:0;
		margin-bottom: 10px;
	}
	.tg1  {
		width: 200px;
		border-collapse:collapse;
		border-spacing:0;
		position: absolute;
		right: 0;
		display: inline-table;
	}
	.tg td{font-family:Arial, sans-serif;font-size:8px;padding:5px 3px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
	.tg th{font-family:Arial, sans-serif;font-size:8px;font-weight:700;padding:10px 3px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
	.tg .tg-yw4l{vertical-align:middle}
	.tg1 td{font-family:Arial, sans-serif;font-size:8px;padding:5px 3px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
	.tg1 th{font-family:Arial, sans-serif;font-size:8px;font-weight:700;padding:5px 3px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
	.tg1 .tg-yw4l{vertical-align:middle}
	tfoot {font-weight:700}
	.tc {text-align:center}
	.tl {text-align:left}
	.tr {text-align:right}
	h4 {font-size:16px;padding-top:0;}
	.subject_footer {
		font-family:Arial, sans-serif;
		position: relative;
		left: 0;
		bottom: 115px;
		font-size: 12px;
		font-weight: 500;
	}
	.subject_footer div {
		font-family:Arial, sans-serif;
		width: 120px;
		float: left;
		margin: 0 60px;
		font-weight: 500;
	}
	.subject_footer .name {
		font-family:Arial, sans-serif;
		margin-top: 50px;
		font-weight: 500;
	}
	.img{
		margin-bottom: 0;
	}
	.title {
		margin-top: 20px;
	}
	.header_image {
		width: 200px;
		background-size: cover;
		float:left;
	}
	.text_title {
		text-align: right;
	}
</style>
<img class="header_image" src="<?php base_url(); ?>assets/img/logo-bri.png"/>
<p class="tc text_title">Jl. Raya Bogor<br/>
Cijantung - Jakarta 14310<br/>
Indonesia<br/><br/></p>

<h4 class="tc title">DATA PINJAMAN<br/></h4>
<table width="100%" class="tg">
  <tr>
    <th width="20px" class="tg-yw4l tc">NO.</th>
    <th width="120px" class="tg-yw4l tc">NAMA NASABAH</th>
    <th width="" class="tg-yw4l tc">NO. KTP</th>
    <th width="" class="tg-yw4l tc">NO. REK</th>
    <th width="" class="tg-yw4l tc">NO. TELP</th>
    <th width="" class="tg-yw4l tc">JENIS KELAMIN</th>
    <th width="" class="tg-yw4l tc">TGL LAHIR</th>
    <th width="" class="tg-yw4l tc">EMAIL</th>
  </tr>
	
	<?php $no = 1; ?>
	<?php foreach($data_nasabah as $nasabah){ ?>

		<tr>
			<td class="tg-yw4l tc"><?php echo $no; ?></td>
			<td class="tg-yw4l tc"><?php echo ucwords($nasabah->nama_lengkap); ?></td>
			<td class="tg-yw4l tc"><?php echo $nasabah->noktp; ?></td>
			<td class="tg-yw4l tc"><?php echo $nasabah->norek; ?></td>
			<td class="tg-yw4l tc"><?php echo $nasabah->notelp; ?></td>
			<td class="tg-yw4l tc"><?php echo get_jenis_kelamin($nasabah->jenis_kelamin); ?></td>
			<td class="tg-yw4l tc"><?php echo $nasabah->tgl_lahir; ?></td>
			<td class="tg-yw4l tc"><?php echo $nasabah->email; ?></td>
		</tr>
	<?php $no++; ?>

  	<?php } ?>

</table>