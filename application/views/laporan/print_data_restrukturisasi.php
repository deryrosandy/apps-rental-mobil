<style type="text/css">
	html { margin: 40px 20px 20px}
	.tg  {
		width: 100%;
		border-collapse:collapse;
		border-spacing:0;
		margin-bottom: 10px;
	}
	.tg1  {
		width: 200px;
		border-collapse:collapse;
		border-spacing:0;
		position: absolute;
		right: 0;
		display: inline-table;
	}
	.tg td{font-family:Arial, sans-serif;font-size:8px;padding:5px 3px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
	.tg th{font-family:Arial, sans-serif;font-size:8px;font-weight:700;padding:10px 3px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
	.tg .tg-yw4l{vertical-align:middle}
	.tg1 td{font-family:Arial, sans-serif;font-size:8px;padding:5px 3px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
	.tg1 th{font-family:Arial, sans-serif;font-size:8px;font-weight:700;padding:5px 3px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
	.tg1 .tg-yw4l{vertical-align:middle}
	tfoot {font-weight:700}
	.tc {text-align:center}
	.tl {text-align:left}
	.tr {text-align:right}
	h4 {font-size:16px;padding-top:0;}
	.subject_footer {
		font-family:Arial, sans-serif;
		position: relative;
		left: 0;
		bottom: 115px;
		font-size: 12px;
		font-weight: 500;
	}
	.subject_footer div {
		font-family:Arial, sans-serif;
		width: 120px;
		float: left;
		margin: 0 60px;
		font-weight: 500;
	}
	.subject_footer .name {
		font-family:Arial, sans-serif;
		margin-top: 50px;
		font-weight: 500;
	}
	.img{
		margin-bottom: 0;
	}
	.title {
		margin-top: 20px;
		line-height:1.4;
	}
	.header_image {
		width: 200px;
		background-size: cover;
		float:left;
	}
	.text_title {
			text-align: right;
	}

</style>

<img class="header_image" src="<?php base_url(); ?>assets/img/logo-bri.png"/>
<p class="tc text_title">Jl. Raya Bogor<br/>
Cijantung - Jakarta 14310<br/>
Indonesia<br/><br/></p>

<?php /* <img class="header_image" src="<?php base_url(); ?>assets/img/header_kop_surat.jpg"/> */ ?>
<h4 class="tc title">Data Pengajuan Restrukturisas Cicilan<br/>

<?php if(!empty($_GET['dari_tanggal'])){ ?>
	PERIODE <?php echo strtoupper(tgl_indo2($_GET['dari_tanggal'])); ?> - <?php echo strtoupper(tgl_indo2($_GET['sampai_tanggal'])); ?>
<?php } ?>

</h4>

<table width="100%" class="tg">
  <tr>
    <th width="20px" class="tg-yw4l tc">NO.</th>
    <th width="100px" class="tg-yw4l tc">NAMA NASABAH</th>
    <th width="60px" class="tg-yw4l tc">NO. REK</th>
    <th width="50px" class="tg-yw4l tc">NO. TELP</th>
    <th width="80px" class="tg-yw4l tc">JUMLAH PINJAMAN</th>
    <th width="20px" class="tg-yw4l tc">TENOR</th>
    <th width="20px" class="tg-yw4l tc">PENANGGUHAN</th>
    <th width="80px" class="tg-yw4l tc">TANGGAL</th>
    <th width="80px" class="tg-yw4l tc">STATUS</th>
  </tr>
	<?php $no = 1; ?>
	<?php $total_penangguhan = 0; ?>
	<?php foreach($data_pengajuan as $pengajuan){ ?>

		<tr>
			<td class="tg-yw4l tc"><?php echo $no; ?></td>
			<td class="tg-yw4l tc"><?php echo ucwords($pengajuan->nama_nasabah); ?></td>
			<td class="tg-yw4l tc"><?php echo $pengajuan->no_rek; ?></td>
			<td class="tg-yw4l tc"><?php echo $pengajuan->no_telp; ?></td>
			<td class="tg-yw4l tc"><?php echo $pengajuan->jml_pinjaman; ?></td>
			<td class="tg-yw4l tc"><?php echo $pengajuan->tenor; ?></td>
			<td class="tg-yw4l tc"><?php echo $pengajuan->lama_penangguhan; ?></td>
			<td class="tg-yw4l tc"><?php echo tgl_indo2($pengajuan->created_at); ?></td>
			<td class="tg-yw4l tc"><?php echo $pengajuan->status; ?></td>
		</tr>

	<?php $no++; ?>

  	<?php } ?>
	
	<?php /*
	<tfoot>
		<td colspan='4' class="tg-yw4l tc">TOTAL</td>
		<td class="tg-yw4l tc"><?php echo format_rupiah($total_transaksi); ?></td>
	</tfoot>
	*/ ?>

</table>