<div class="msg" style="display:none;">
	<?php echo @$this->session->flashdata('msg'); ?>
</div>

<div class="row">
	<div class="col-md-8">
		<div class="box box-primary">
			<div class="box-header with-border">
				<h3 class="box-title">Detail Data Pengajuan Restrukturisasi </h3>
			</div>
			<!-- /.box-header -->
			<div class="row">
				<div class="col-md-12 col-md-offset-0">
					<div class="col-md-12 col-md-offset-0">

						<table class="table table-condensed">
							<tbody>
								<tr>
									<td><strong>Nama Nasabah</span></strong></td>
									<td><?php echo $data_pengajuan->nama_nasabah; ?></span></td>
								</tr>
								<tr>
									<td><strong>No. Telp</span></strong></td>
									<td><?php echo $data_pengajuan->no_telp; ?></td>
								</tr>
								<tr>
									<td><strong>No. Rekening</span></strong></td>
									<td><?php echo $data_pengajuan->no_rek; ?></td>
								</tr>
								<tr>
									<td><strong>Jumlah Pinjaman</span></strong></td>
									<td>
										<?php echo format_rupiah($data_pengajuan->jml_pinjaman); ?>
									</td>
								</tr>
								<tr>
									<td><strong>Tenor</strong></td>
									<td>
										<?php echo $data_pengajuan->tenor ?>
									</td>
								</tr>
								<tr>
									<td><strong>Cicilan Perbulan</strong></td>
									<td>
										<?php echo format_rupiah($data_pengajuan->cicilan_perbulan); ?>
									</td>
								</tr>
								<tr>
									<td><strong>Jenis Pinjaman</strong></td>
									<td>
										<?php echo $data_pengajuan->jenis_pinjaman ?>
									</td>
								</tr>
								<tr>
									<td><strong>Tgl Mulai Pinjaman</strong></td>
									<td>
										<?php echo tgl_indo2($data_pengajuan->tgl_pinjaman); ?>
									</td>
								</tr>
								<tr>
									<td><strong>Cicilan Ke</strong></td>
									<td>
										<?php echo $data_pengajuan->cicilan_ke; ?>
									</td>
								</tr>
								<tr>
									<td><strong>Alamat</strong></td>
									<td>
										<?php echo ucfirst($data_pengajuan->alamat); ?>
									</td>
								</tr>
								<tr>
									<td><strong>Lama Penangguhan</strong></td>
									<td>
										<?php echo ucfirst($data_pengajuan->lama_penangguhan) . ' Bulan'; ?> 
									</td>
								</tr>
								<tr>
									<td><strong>Cicilan Selama Penangguhan</strong></td>
									<td>
										<?php echo format_rupiah($data_pengajuan->suku_bunga); ?> 
									</td>
								</tr>
								<tr>
									<td><strong>Keterangan</strong></td>
									<td>
										<?php echo ucfirst($data_pengajuan->keterangan); ?>
									</td>
								</tr>
								<tr>
									<td><strong>Status</strong></td>
									<td>
										<?php echo status_pengajuan($data_pengajuan->status_pengajuan); ?>
									</td>
								</tr>
								<?php if($this->userdata->user_type=='admin'){ ?>
									<tr>
										<td>
											<div class="row">
												<div class="btn-group btn-lg">
													<a <?php echo ($data_pengajuan->status_pengajuan != 'open' ? 'disabled' : ''); ?> href="javascript:void(0);"  data-url="<?php echo site_url('restrukturisasi/approve')?>" data-id="<?php echo $data_pengajuan->pengajuan_id; ?>" title="Approved" class="btn btn-md btn-primary <?php echo ($data_pengajuan->status_pengajuan != 'open' ? '' : 'approve'); ?>"><i class="fa fa-check"> Approve</i></a>
													<a <?php echo ($data_pengajuan->status_pengajuan != 'open' ? 'disabled' : ''); ?> href="javascript:void(0);" data-url="<?php echo site_url('restrukturisasi/reject')?>" data-id="<?php echo $data_pengajuan->pengajuan_id; ?>" title="Reject" class="btn btn-md btn-danger <?php echo ($data_pengajuan->status_pengajuan != 'open' ? '' : 'reject'); ?>"><i class="fa fa-close"> Reject</i></a>
												</div>
											</div>
										</td>
										<td>

										</td>
									</tr>
								<?php } ?>
							</tbody>
						</table>

					</div>
				</div>
			</div>
		</div>
	</div>
</div>