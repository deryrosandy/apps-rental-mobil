<div class="msg">
	<?php echo @$this->session->flashdata('msg'); ?>
</div>

<div class="row">
	<div class="col-md-12">
		<div class="box box-primary">
			<div class="box-header with-border">
				<h3 class="box-title">Form Restrukturisasi</h3>
			</div>
			<!-- /.box-header -->
			<div class="row">
				<div class="col-md-10">
					<!-- form start -->
					<form action="<?php echo base_url(); ?>restrukturisasi/submit_pengajuan" class="form-horizontal" id="" method="post" enctype="multipart/form-data">
						<div class="box-body">

							<div class="form-group">
								<label class="control-label col-xs-4" for="nasabah_id">Nasabah</label>
								<div class="col-md-7">
									<select name="nasabah_id" class="form-control select2" id="pilih_nasabah" required>
										<option value="">- Pilih Nasabah -</option>
										<?php foreach ($data_nasabah as $nasabah) { ?>
											<option value="<?php echo $nasabah->nasabah_id; ?>"><?php echo $nasabah->nama_lengkap; ?></option>
										<?php } ?>
									</select>
								</div>
							</div>

							<div class="form-group">
								<label for="no_rek" class="col-sm-4 control-label">No. Rekening</label>

								<div class="col-sm-7">
									<input type='text' class="form-control no_rek" name="no_rek" readonly/>
								</div>
							</div>

							<div class="form-group">
								<label for="jml_pinjaman" class="col-sm-4 control-label">Jumlah Pinjaman</label>

								<div class="col-sm-7">
									<input type='text' class="form-control jml_pinjaman" name="jml_pinjaman" readonly />
								</div>
							</div>

							<div class="form-group">
								<label for="tenor" class="col-sm-4 control-label">Tenor Pinjaman (Bulan)</label>

								<div class="col-sm-7">
									<input type='text' class="form-control tenor" name="tenor" readonly />
								</div>
							</div>

							<div class="form-group">
								<label for="tenor" class="col-sm-4 control-label">Cicilan Perbulan</label>

								<div class="col-sm-7">
									<input type='text' class="form-control jml_cicilan_pokok" readonly />
								</div>
							</div>

							<div class="form-group">
								<label class="control-label col-xs-4" for="lama_penangguhan">Lama Penangguhan</label>
								<div class="col-md-7">
									<select name="lama_penangguhan" class="form-control" id="lama_penangguhan" required>
										<option>- Pilih Salah Satu -</option>
										<option value="1">1 Bulan</option>
										<option value="2">3 Bulan</option>
										<option value="6">6 Bulan</option>
									</select>
								</div>
							</div>
							
							<div class="form-group">
								<label for="keterangan" class="col-sm-4 control-label">Keterangan</label>

								<div class="col-sm-7">
									<textarea class="form-control" row="4" name="keterangan"></textarea>
								</div>
							</div>

						</div>
						<!-- /.box-body -->

						<div class="box-footer">
							<div class="col-sm-4">

							</div>
							<div class="col-sm-7">
								<div class="row">
									<button type="submit" id="btn_submit_cuti" class="btn btn-primary pull-right">Simpan</button>
									<a onclick="history.back(-1);" class="btn btn-default pull-right" style="margin-right: 5px;">Cancel</a>
								</div>
							</div>

							<div class="col-sm-4">
								&nbsp;
							</div>
						</div>
						<!-- /.box-footer -->
					</form>
				</div>
			</div>
		</div>
	</div>
</div>