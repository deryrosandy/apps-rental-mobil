<div class="msg">
	<?php echo @$this->session->flashdata('msg'); ?>
</div>

<div class="row">
	<div class="col-md-12">
		<div class="box box-primary">
			<div class="box-body">
				<table id="datatable1" class="table table-striped">
					<thead>
						<tr>
							<th>No.</th>
							<th>Nama Nasabah</th>
							<th>Norek</th>
							<th>No. Telp</th>
							<th>Tenor</th>
							<th>Jumlah Pinjaman</th>
							<th>Lama Penangguhan</th>
							<th>Status</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						<?php $no = 1; ?>
						<?php if($data_pengajuan): ?>
						<?php foreach ($data_pengajuan as $data) { ?>
							<tr>
								<td><?php echo  $no; ?></td>
								<td><?php echo  $data->nama_lengkap; ?></td>
								<td><?php echo  $data->norek; ?></td>
								<td><?php echo  $data->notelp; ?></td>
								<td><?php echo  $data->tenor; ?></td>
								<td><?php echo  $data->jml_pinjaman; ?></td>
								<td><?php echo  $data->lama_penangguhan; ?> Bulan</td>
								<td><?php echo  status_pengajuan($data->status); ?></td>
								<td>
									<div class="btn-group btn-sm btn-xs">
										<a href="<?php echo base_url() ?>restrukturisasi/detail/<?php echo $data->pengajuan_id; ?>" title="Detail" class="btn btn-xs btn-primary"><i class="fa fa-file-text"></i></a>
										<a href="<?php echo base_url() ?>restrukturisasi/print_restrukturisasi/<?php echo $data->pengajuan_id; ?>" target="_blank" data-id="<?php echo $data->pengajuan_id; ?>" title="Print" class="btn btn-xs btn-info"><i class="fa fa-print"></i></a>
										<?php if($this->userdata->user_type=='admin'){ ?>
											<a href="javascript:void(0);" data-id="<?php echo $data->pengajuan_id; ?>" data-url="<?php echo site_url('restrukturisasi/delete')?>" class="btn btn-xs btn-danger delete" title="" data-original-title="hapus"><i class="fa fa-trash"></i></a>
										<?php } ?>
									</div>
								</td>
							</tr>
							<?php $no++; ?>
						<?php } ?>
						<?php endif; ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>

</div>