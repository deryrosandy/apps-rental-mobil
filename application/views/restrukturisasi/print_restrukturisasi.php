<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>.:: Data Restrukturisas Cicilan ::.</title>
		
        <style type="text/css">
		.edit-mode-wrap {
			padding: 0;
			margin:0;
		}
		.totals-row td {
			border-right:none !important;
			border-left:none !important;
		}
		
		.header_image {
			width: 200px;
			background-size: cover;
		}
		.text_title {
			text-align: right;
		}
		
		td {
			white-space: nowrap;
		}
		.items-table td ,#notes { white-space:normal;}
		.totals-row td strong,.items-table th {
			white-space:nowrap;
		}
		</style>
		<style type="text/css">
			.is_logo {display:none;}
		</style>

	</head>
	<body>
		<div id="editor" class="edit-mode-wrap">
			<style type="text/css">
				</style><style type="text/css">
					* { 
						margin:0;
						padding:0; 
					}
					body { 
						background:#fff;
						font-family:Arial, Helvetica, sans-serif;
						font-size:14px;
						line-height:20px;
						padding:0;
						height: 100%;
					}
					#extra {text-align: center; font-size: 22px;  font-weight: 700; margin-bottom:15px;margin-top:15px;}
					.invoice-wrap { width:100%; margin:0 auto; background:#FFF; color:#000;}
					.invoice-inner { margin:0; padding-top:0; padding:10px }
					.invoice-address { border-top: 3px double #000000; margin: 15px 0; padding-top: 10px; padding-bottom:10px;}
					.bussines-name { font-size:18px; font-weight:100 }
					.invoice-name { font-size:22px; font-weight:700 }
					.listing-table th { background-color: #ffffff; border-bottom: 1px solid #555555; border-top: 1px solid #555555; font-weight: bold; text-align:left; padding:6px 4px }
					.listing-table td { border-bottom: 1px solid #555555; text-align:left; padding:5px 6px; vertical-align:top }
					.total-table td { border-left: 1px solid #555555; }
					.total-row { background-color: #ffffff; border-bottom: 1px solid #555555; border-top: 1px solid #555555; font-weight: bold; }
					.row-items { margin:0 0; display:block }
					.notes-block {
						margin:50px 0 0 0 }
					/*tables*/
					table td { vertical-align:top}
					.items-table { border:1px solid #1px solid #555555; border-collapse:collapse; width:100%}
					.items-table td, .items-table th { border:1px solid #555555; padding:4px 5px ; text-align:left}
					.items-table th { background:#f5f5f5;}
					.totals-row .wide-cell { border:1px solid #fff; border-right:1px solid #555555; border-top:1px solid #555555}
				</style>
				<div class="invoice-wrap">
					<div class="invoice-inner">
						
						<table cellspacing="0" cellpadding="0" border="0" width="100%">
							<tbody>
								<tr>
									<td valign="top" align="right">
									<div class="business_info">
									<table cellspacing="0" cellpadding="0" border="0" width="100%">
										<tbody>
											<tr>
												<td>
													<span class="editable-area" style="width:100%;"id="business_info">
														<img class="header_image" src="<?php base_url(); ?>assets/img/logo-bri.png"/>
														<br/></p>
													</span>
												</td>
											</tr>
										</tbody>
									</table>
									</div>
									</td>
									<td valign="top" align="right">
									<p class="tc text_title">Jl. Raya Bogor<br/>
										Cijantung - Jakarta 14310<br/>
										Indonesia<br/><br/></p>									
									</td>
								</tr>
							</tbody>
						</table>

						<table cellspacing="0" cellpadding="0" border="0" width="100%">
							<tbody>
								<tr>
									<td valign="top" align="center">
										<p class="editable-text" id="extra"><span style="font-size: 14pt; text-align:center;">Data Restrukturisas Cicilan</span></p>							
									</td>
								</tr>
							</tbody>
						</table>

						<div class="invoice-address"  style="padding-top:20px;">
							<table class="listing-table" cellspacing="0" cellpadding="0" border="0" width="100%">
								<tbody>
									<tr>
										<td width="40%" align="right"><strong><span class="editable-text" id="label_invoice_no">Nama Nasabah</span></strong></td>
										<td width="60%" style="padding-left:20px" align="left"><span class="editable-text" id="no">:&nbsp;&nbsp;<?php echo ucwords($pengajuan->nama_nasabah); ?></span></td>
									</tr>
									<tr>
										<td align="right"><strong><span class="editable-text" id="label_date">No. Telp</span></strong></td>
										<td style="padding-left:20px" align="left"><span class="editable-text" id="date">:&nbsp;&nbsp;<?php echo tgl_indo($pengajuan->no_telp); ?></span></td>
									</tr>
									<tr>
										<td align="right"><strong><span class="editable-text" id="label_date">No. Rekening</span></strong></td>
										<td style="padding-left:20px" align="left"><span class="editable-text" id="date">:&nbsp;&nbsp;<?php echo $pengajuan->no_rek; ?></span></td>
									</tr>
									<tr>
										<td align="right"><strong><span class="editable-text" id="label_date">Jumlah Pinjaman</span></strong></td>
										<td style="padding-left:20px" align="left"><span class="editable-text">:&nbsp;&nbsp;<?php echo format_rupiah($pengajuan->jml_pinjaman); ?></span></td>
									</tr>
									<tr>
										<td align="right"><strong><span class="editable-text" id="label_date">Tenor</span></strong></td>
										<td style="padding-left:20px" align="left"><span class="editable-text">:&nbsp;&nbsp;<?php echo ($pengajuan->tenor); ?> Bulan</span></td>
									</tr>
									<tr>
										<td align="right"><strong><span class="editable-text" id="label_date">Cicilan Ke</span></strong></td>
										<td style="padding-left:20px" align="left"><span class="editable-text">:&nbsp;&nbsp;<?php echo ($pengajuan->cicilan_ke); ?></span></td>
									</tr>
									<tr>
										<td align="right"><strong><span class="editable-text" id="label_date">Jenis Pinjaman</span></strong></td>
										<td style="padding-left:20px" align="left"><span class="editable-text">:&nbsp;&nbsp;<?php echo ucfirst($pengajuan->jenis_pinjaman); ?></span></td>
									</tr>
									<tr>
										<td align="right"><strong><span class="editable-text" id="label_date">Lama Penangguhan</span></strong></td>
										<td style="padding-left:20px" align="left"><span class="editable-text">:&nbsp;&nbsp;<?php echo $pengajuan->lama_penangguhan; ?></span></td>
									</tr>
									<tr>
										<td align="right"><strong><span class="editable-text" id="label_date">Cicilan Selama Penangguhan</span></strong></td>
										<td style="padding-left:20px" align="left"><span class="editable-text">:&nbsp;&nbsp;<?php echo format_rupiah($pengajuan->suku_bunga); ?></span></td>
									</tr>
									<tr>
										<td align="right"><strong><span class="editable-text" id="label_date">Tanggal Pengajuan</span></strong></td>
										<td style="padding-left:20px" align="left"><span class="editable-text">:&nbsp;&nbsp;<?php echo tgl_indo2($pengajuan->created_at); ?></span></td>
									</tr>
									<tr>
										<td align="right"><strong><span class="editable-text" id="label_date">Keterangan</span></strong></td>
										<td style="padding-left:20px" align="left"><span class="editable-text">:&nbsp;&nbsp;<?php echo ($pengajuan->keterangan); ?></span></td>
									</tr>
									<tr>
										<td align="right"><strong><span class="editable-text" id="label_date">Status Pengajuan</span></strong></td>
										<td style="padding-left:20px" align="left"><span class="editable-text">:&nbsp;&nbsp;<?php echo status_pengajuan($pengajuan->status_pengajuan); ?></span></td>
									</tr>
								</tbody>
							</table>
						</div>
						<?php /*
						<div id="items-list">
							<table class="table table-bordered table-condensed table-striped items-table">
								<thead>
									<tr>
										<th>No.</th>
										<th>Kode Barang</th>
										<th>Nama Barang</th>
										<th>Qty</th>
									</tr>
								</thead>
								<tbody>

								<?php $no = 1; ?>
								<?php foreach($product_list as $product){ ?>
									<tr>
										<td><?php echo $no; ?></td>
										<td><?php echo $product->product_code; ?></td>
										<td><?php echo $product->product_name; ?></td>
										<td><?php echo $product->quantity; ?></td>
									</tr>
									<?php $no++; ?>
								<?php } ?>

								</tbody>
							</table>
						</div>
						*/ ?>
						<!--
						<div class="notes-block">
							<div class="editable-area" id="notes" style=""><b>Perhatian:</b></div>
							<div class="notice">1. Pengambilan barang harap di sertai nota</div>
						</div>
						-->
					</div>
				</div>
			</div>
		<style>
body {
    background: #ffffff;
	padding:0;
}
.invoice-wrap {
	box-shadow: 0 0 4px rgba(0, 0, 0, 0.1);
}
#mobile-preview-close a {
	position:fixed; 
	left:20px;
	bottom:30px; 
	background-color: #fff;
	font-weight: 600;
	outline: 0 !important;
	line-height: 1.5;
	border-radius: 3px;
	font-size: 14px;
	padding: 7px 10px;
	border:1px solid #fff;
	text-decoration:none;
}
#mobile-preview-close img {
	width:20px;
	height:auto;
}
#mobile-preview-close a:nth-child(2) {
left:190px;
background:#f5f5f5;
border:1px solid #9f9f9f;
color:#555 !important;
}
#mobile-preview-close a:nth-child(2) img {
    height: auto;
	position: relative;
	top: 2px;
}
.invoice-wrap {padding: 20px;}


@media print {
  #mobile-preview-close a {
  display:none
}
body {
    background: none;
}
.invoice-wrap {box-shadow: none; margin-bottom: 0px;}

}
</style>

</body>

</html>