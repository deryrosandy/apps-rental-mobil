<?php /*
<div class="msg">
	<div class="alert alert-danger alert-dismissible">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
		<h4><i class="icon fa fa-check"></i> Data Berhasil di Masukkan!</h4>
	</div>
</div>
*/ ?>

<div class="row">
	<div class="col-md-12">
		<div class="box box-primary">
			<div class="box-header with-border">
				<h3 class="box-title">Form Restrukturisasi</h3>
			</div>
			<!-- /.box-header -->
			<div class="row">
				<div class="col-md-10">

					<?php if ($data_pinjaman) : ?>
						<!-- form start -->

						<?php if (!empty($pengajuan)) : ?>

							<div class="box-body">
								<div class="form-group">
									<div class="col-sm-11">
										<div class="alert alert-info info-dismissible">
											<h4><i class="icon fa fa-info-circle"></i> Anda sudah melakukan permintaan restrukturisasi pinjaman</h4>
										</div>
									</div>
								</div>
							</div>

						<?php endif; ?>

						<form action="<?php echo base_url(); ?>restrukturisasi/submit_pengajuan_nasabah" class="form-horizontal" id="" method="post" enctype="multipart/form-data">
							<div class="box-body">

								<input type='hidden' class="form-control" name="nasabah_id" value="<?php echo $data_pinjaman->nasabah_id; ?>" />
								<input type='hidden' class="form-control" name="nama_nasabah" value="<?php echo $data_pinjaman->nama_lengkap; ?>" />
								<input type='hidden' class="form-control" name="pinjaman_id" value="<?php echo $data_pinjaman->pinjaman_id; ?>" />
								<input type='hidden' class="form-control" name="no_telp" value="<?php echo $data_pinjaman->notelp; ?>" />
								<input type='hidden' class="form-control" name="alamat" value="<?php echo $data_pinjaman->alamat; ?>" />

								<div class="form-group">
									<label for="no_rek" class="col-sm-4 control-label">No. Rekening</label>

									<div class="col-sm-7">
										<input type='text' class="form-control no_rek" name="no_rek" value="<?php echo $data_pinjaman->no_rek; ?>" readonly />
									</div>
								</div>

								<div class="form-group">
									<label for="jml_pinjaman" class="col-sm-4 control-label">Jumlah Pinjaman</label>

									<div class="col-sm-7">
										<input type='text' class="form-control" value="<?php echo format_digit($data_pinjaman->jml_pinjaman); ?>" readonly />
										<input type='hidden' class="form-control jml_pinjaman" name="jml_pinjaman" value="<?php echo $data_pinjaman->jml_pinjaman; ?>" readonly />
									</div>
								</div>

								<div class="form-group">
									<label for="tenor" class="col-sm-4 control-label">Tenor Pinjaman (Bulan)</label>

									<div class="col-sm-7">
										<input type='text' class="form-control tenor" value="<?php echo $data_pinjaman->tenor; ?>" name="tenor" readonly />
									</div>
								</div>

								<div class="form-group">
									<label for="tenor" class="col-sm-4 control-label">Cicilan Perbulan</label>

									<div class="col-sm-7">
										<input type='text' class="form-control jml_cicilan_pokok" value="<?php echo format_digit($data_pinjaman->cicilan_perbulan); ?>" readonly />
										<input type='hidden' class="form-control jml_cicilan_pokok" value="<?php echo $data_pinjaman->cicilan_perbulan; ?>" readonly />
									</div>
								</div>

								<div class="form-group">
									<label class="control-label col-xs-4" for="lama_penangguhan">Lama Penangguhan</label>
									<div class="col-md-7">
										<select <?php echo (!empty($pengajuan) ? "readonly" : ""); ?> name="lama_penangguhan" class="form-control" id="lama_penangguhan" required>
											<option>- Pilih Salah Satu -</option>
											<option value="1">1 Bulan</option>
											<option value="2">3 Bulan</option>
											<option value="6">6 Bulan</option>
										</select>
									</div>
								</div>

								<div class="form-group">
									<label for="keterangan" class="col-sm-4 control-label">Keterangan</label>

									<div class="col-sm-7">
										<textarea <?php echo (!empty($pengajuan) ? "readonly" : ""); ?> class="form-control" row="4" name="keterangan"></textarea>
									</div>
								</div>

							</div>
							<!-- /.box-body -->

							<div class="box-footer">
								<div class="col-sm-4">
								</div>
								<div class="col-sm-7">
									<div class="row">
										<button <?php echo (!empty($pengajuan) ? "disabled" : ""); ?> type="submit" id="btn_submit_cuti" class="btn btn-primary pull-right">Simpan</button>
										<a onclick="history.back(-1);" class="btn btn-default pull-right" style="margin-right: 5px;">Cancel</a>
									</div>
								</div>

								<div class="col-sm-4">
									&nbsp;
								</div>
							</div>
							<!-- /.box-footer -->

						</form>
					<?php else : ?>
						<div class="box-body">
							<div class="alert alert-warning warning-dismissible">
								<h4><i class="icon fa fa-check"></i> Anda Belum Mempunyai Data Pinjaman</h4>
							</div>
						</div>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</div>
</div>