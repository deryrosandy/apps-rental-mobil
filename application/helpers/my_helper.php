<?php

/**
 * Check if Result Query more or one row
 *
 * @param unknown $query        	
 * @return
 *
 */
function checkRes($query) {
	if ($query->num_rows() > 0) {
		return $query->result();
	} else {
		return false;
	}
}

/**
 * Check if Result Query has one row
 *
 * @param unknown $query        	
 * @return boolean
 */
function checkRow($query) {
	if ($query->num_rows() > 0) {
		return $query->row();
	} else {
		return false;
	}
}

/**
 * Count Row
 *
 * @param unknown $query        	
 * @return boolean
 */
function countRow($query) {
	if ($query->num_rows() > 0) {
		return $query->num_rows();
	} else {
		return false;
	}
}

/**
 * Only Number Allow
 *
 * @param unknown $num        	
 * @return mixed
 */
function numberOnly($num) {
	return preg_replace('/\D/', '', $num);
}

// MSG
function show_msg($content='', $type='success', $icon='fa-info-circle', $size='14px') {
	if ($content != '') {
				
		return  '<div class="alert alert-' .$type .'alert-dismissible">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					<h4><i class="icon fa ' .$icon .'"></i> ' .$content . '!</h4>
				</div>';
	}
}

function show_succ_msg($content='', $size='14px') {
	if ($content != '') {
		return  '<div class="alert alert-success alert-dismissible">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					<h4><i class="icon fa fa-check"></i> ' .$content . '!</h4>
				</div>';
	}
}

function show_err_msg($content='', $size='14px') {
	if ($content != '') {
		return  '<div class="alert alert-danger alert-dismissible">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					<h4><i class="icon fa fa-warning"></i> ' .$content . '!</h4>
				</div>';
	}
}

// MODAL
function show_my_modal($content='', $id='', $data='', $size='md') {
	$_ci = &get_instance();

	if ($content != '') {
		$view_content = $_ci->load->view($content, $data, TRUE);

		return '<div class="modal fade" id="' .$id .'" role="dialog">
				  <div class="modal-dialog modal-' .$size .'" role="document">
					<div class="modal-content">
						' .$view_content .'
					</div>
				  </div>
				</div>';
	}
}

function show_my_confirm($id='', $class='', $title='Konfirmasi', $yes = 'Ya', $no = 'Tidak') {
	$_ci = &get_instance();

	if ($id != '') {
		echo   '<div class="modal fade" id="' .$id .'" role="dialog">
				  <div class="modal-dialog modal-md" role="document">
					<div class="modal-content">
						<div class="col-md-offset-1 col-md-10 col-md-offset-1 well">
						  <h3 style="display:block; text-align:center;">' .$title .'</h3>
						  
						  <div class="col-md-6">
							<button class="form-control btn btn-primary ' .$class .'"> <i class="glyphicon glyphicon-ok-sign"></i> ' .$yes .'</button>
						  </div>
						  <div class="col-md-6">
							<button class="form-control btn btn-danger" data-dismiss="modal"> <i class="glyphicon glyphicon-remove-sign"></i> ' .$no .'</button>
						  </div>
						</div>
					</div>
				  </div>
				</div>';
	}
}

function tgl_indo($tgl){
		$tanggal = substr($tgl,8,2);
		$bulan = getBulan(substr($tgl,5,2));
		$tahun = substr($tgl,0,4);
		return $tanggal.' '.$bulan.' '.$tahun;		 
}

function getBulan($bln){
	switch ($bln){
		case 1: 
			return "Januari";
			break;
		case 2:
			return "Februari";
			break;
		case 3:
			return "Maret";
			break;
		case 4:
			return "April";
			break;
		case 5:
			return "Mei";
			break;
		case 6:
			return "Juni";
			break;
		case 7:
			return "Juli";
			break;
		case 8:
			return "Agustus";
			break;
		case 9:
			return "September";
			break;
		case 10:
			return "Oktober";
			break;
		case 11:
			return "November";
			break;
		case 12:
			return "Desember";
			break;
	}
}

function tgl_indo2($tgl){
		$tanggal = substr($tgl,8,2);
		$bulan = getBulan2(substr($tgl,5,2));
		$tahun = substr($tgl,0,4);
		return $tanggal.' '.$bulan.' '.$tahun;		 
}

function getHour($hour){
	$jam = substr($hour,10,6);
	return $jam;	
}

function getBulan2($bln){
	switch ($bln){
		case 1: 
			return "Jan";
			break;
		case 2:
			return "Feb";
			break;
		case 3:
			return "Mar";
			break;
		case 4:
			return "Apr";
			break;
		case 5:
			return "Mei";
			break;
		case 6:
			return "Jun";
			break;
		case 7:
			return "Jul";
			break;
		case 8:
			return "Ags";
			break;
		case 9:
			return "Sep";
			break;
		case 10:
			return "Okt";
			break;
		case 11:
			return "Nov";
			break;
		case 12:
			return "Des";
			break;
	}
}

function getHari($tgl){

	$day = date('D', strtotime($tgl));

	if ($day=='Sun'){
		return 'Minggu';
	}elseif($day=='Mon'){
		return 'Senin';
	}elseif($day=='Tue'){
		return 'Selasa';
	}elseif($day=='Wed'){
		return 'Rabu';
	}elseif($day=='Thu'){
		return 'Kamis';
	}elseif($day=='Fri'){
		return 'Jumat';
	}elseif($day=='Sat'){
		return 'Sabtu';
	}else{
		return 'ERROR!';
	}
}

function format_rupiah($angka){
	if($angka != null || $angka != '0'){
		$rupiah = "Rp. " . number_format($angka,0,',','.');
	}else{
		$rupiah = $angka;
	}
	return $rupiah;
}

function format_digit($angka){
	//var_dump($angka); 
	if($angka == '0' || $angka == null){
		return '- &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
	}else{
		$digit = number_format(str_replace(' ', '', $angka),0,',','.');
		return $digit;
	}
}

function format_decimal($angka){
	$digit = number_format($angka,2,',','.');
	return $digit;
}

function status_active($status){
	switch ($status) {
		case "1":
			return '<label class="label label-success">Active</label>';
			break;
		case "0":
			return '<label class="label label-danger">Non Active</label>';
			break;
	}
}

function status_pengajuan($status){
	switch ($status) {
		case "open":
			return '<label class="label label-primary">Open</label>';
			break;
		case "approved":
			return '<label class="label label-success">Approved</label>';
			break;
		case "reject":
			return '<label class="label label-danger">Reject</label>';
			break;
		case "closed":
			return '<label class="label label-warning">Closed</label>';
			break;
	}
}

function get_nama_nasabah($nasabah_id){
	$CI = get_instance();
	$CI->load->model('Global_model');

	$nasabah = $CI->Global_model->get_nasabah_by_nasabah_id($nasabah_id);
	
	return $nasabah->nama_lengkap;
}

function get_jabatan_name($kd_jabatan){
	$CI = get_instance();
	$CI->load->model('Global_model');

	$category_name = $CI->Global_model->get_jabatan_name_by_id($kd_jabatan);
	
	return $category_name;
}

function get_divisi_name($divisi_id){
	$CI = get_instance();
	$CI->load->model('Global_model');

	$divisi_name = $CI->Global_model->get_divisi_name_by_id($divisi_id);
	
	return $divisi_name;
}

function get_area_kerja_name($area_kerja){
	$CI = get_instance();
	$CI->load->model('Global_model');

	$area_name = $CI->Global_model->get_area_kerja_by_id($area_kerja);
	
	return $area_name;
}

function get_jenis_cuti_name($kd_jenis){
	$CI = get_instance();
	$CI->load->model('Global_model');

	$jenis_name = $CI->Global_model->get_jenis_cuti_name_by_id($kd_jenis);
	
	if($jenis_name != null){
		$jenis_name = $jenis_name;
	}else{
		$jenis_name = '-';
	}

	return $jenis_name;
}

function get_kategori_name($kd_category){
	$CI = get_instance();
	$CI->load->model('Global_model');

	$jenis_name = $CI->Global_model->get_category_name_by_id($kd_category);
	
	return $jenis_name;
}

function get_status_name($status){
	switch ($status){
		case "0": 
			return "<label class='label label-danger'>Non Active</label>";
			break;
		case "1":
		return "<label class='label label-primary'>Active</label>";
			break;
	}
}

function get_jenis_kelamin($jenis_kel){
	switch ($jenis_kel){
		case "L": 
			return "Laki-laki";
			break;
		case "P": 
			return "Perempuan";
			break;
	}
}

function get_gross_total_gaji($kodekar, $dari_tanggal, $sampai_tanggal, $branch_id){
	$CI = get_instance();
	$CI->load->model('Global_model');
	$CI->load->model('M_Komisi_lulur');
	//Cari komisi Lulur
	$total_sesi = $CI->Global_model->get_total_session_by_kodekar($kodekar, $dari_tanggal, $sampai_tanggal, $branch_id);
	$kodetrp 	=  $CI->Global_model->get_kodetrp_by_kodekar($kodekar, $branch_id);
	$levelkomisi 	=  $CI->Global_model->get_level_komisi_by_kodekar($kodekar, $kodetrp);
	$skema_komisi	 =  $CI->M_Komisi_lulur->get_skema_komisi_lulur($total_sesi->total_point, $levelkomisi);	
	$total_komisi_premium = ($total_sesi->total_pre)*10000;
	$total_komisi_lulur = ($total_sesi->total_sesi * $skema_komisi);

	//Cari komisi Add
	$total_add = $CI->Global_model->get_total_additional_by_kodekar($kodekar, $dari_tanggal, $sampai_tanggal, $branch_id);

	//Cari Komisi Lain-lain
	$komisi_lain_lain = $CI->Global_model->get_komisi_lain_by_kodekar($dari_tanggal, $sampai_tanggal, $kodekar, $branch_id);

	//Cari Komisi FNB
	$total_fnb = $CI->Global_model->get_komisi_fnb_by_kodekar($dari_tanggal, $sampai_tanggal, $kodekar, $branch_id);

	//Cari Komisi Kuesioner
	//$total_kuesioner = $CI->Global_model->get_komisi_kuesioner_by_kodekar($dari_tanggal, $sampai_tanggal, $kodekar);

	if($komisi_lain_lain !== null){
		$a = intval($komisi_lain_lain->jumlah_komisi);
	}else{
		$a = 0;
	}

	$b = intval($total_komisi_lulur)+intval($total_komisi_premium);
	$d = intval($total_fnb->jumlah_komisi);
	//$e = intval($total_kuesioner->jumlah_komisi);

	if($total_add !== null){
		$c = intval($total_add->total_komisi_additional);
	}else{
		$c = 0;
	}
	
	$total_gaji = $a+$b+$c+$d;
	
	return $total_gaji;
}

function get_total_gaji($kodekar, $dari_tanggal, $sampai_tanggal, $branch_id){
	$CI = get_instance();
	$CI->load->model('Global_model');

	$gross_total = get_gross_total_gaji($kodekar, $dari_tanggal, $sampai_tanggal, $branch_id);
	$potongan_gaji = get_potongan_gaji($kodekar, $dari_tanggal, $sampai_tanggal, $branch_id);
	
	$total_potongan = $potongan_gaji->yayasan+$potongan_gaji->asuransi+$potongan_gaji->koperasi+$potongan_gaji->mess+$potongan_gaji->hutang+$potongan_gaji->denda+$potongan_gaji->zion+$potongan_gaji->lain_lain+$potongan_gaji->trt_glx;
	$total_gaji = intval($gross_total)-intval($total_potongan);
	//var_dump($total_potongan); die();
	return $total_gaji;
}

function get_takehomepay($kodekar, $dari_tanggal, $sampai_tanggal, $branch_id){
	$CI = get_instance();
	$CI->load->model('Global_model');

	$gross_total = get_gross_total_gaji($kodekar, $dari_tanggal, $sampai_tanggal, $branch_id);
	$potongan_gaji = get_potongan_gaji($kodekar, $dari_tanggal, $sampai_tanggal, $branch_id);
	//$total_kuesioner = $CI->Global_model->get_komisi_kuesioner_by_kodekar($dari_tanggal, $sampai_tanggal, $kodekar);
	$total_potongan = $potongan_gaji->yayasan+$potongan_gaji->asuransi+$potongan_gaji->koperasi+$potongan_gaji->mess+$potongan_gaji->hutang+$potongan_gaji->denda+$potongan_gaji->zion+$potongan_gaji->lain_lain+$potongan_gaji->trt_glx;
	$total_gaji = intval($gross_total)-intval($total_potongan);
	//var_dump($total_potongan); die();
	return $total_gaji;
}

function get_kuesioner($dari_tanggal, $sampai_tanggal, $kodekar, $branch_id){
	$CI = get_instance();
	$CI->load->model('Global_model');

	$total_kuesioner = $CI->Global_model->get_komisi_kuesioner_by_kodekar($dari_tanggal, $sampai_tanggal, $kodekar, $branch_id);
	//var_dump($total_kuesioner); die();
	return $total_kuesioner;
}

function get_potongan_gaji($kodekar, $dari_tanggal, $sampai_tanggal, $branch_id){
	$CI = get_instance();
	$CI->load->model('Global_model');
	$potongan = $CI->Global_model->get_potongan_by_kodekar($kodekar, $dari_tanggal, $sampai_tanggal, $branch_id);
	
	return $potongan;
}

function get_tabungan($kodekar, $dari_tanggal, $sampai_tanggal,  $branch_id){
	$CI = get_instance();
	$CI->load->model('Global_model');
	$tabungan = $CI->Global_model->get_tabungan_by_kodekar($kodekar, $dari_tanggal, $sampai_tanggal, $branch_id);
	//var_dump($tabungan); die();
	return $tabungan;
}

?>