<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Master extends AUTH_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model(array('Global_model'));
	}

	public function index()
	{

		$this->data_users();
	}

	public function data_users()
	{

		$data['userdata'] 		= $this->userdata;

		$data['users'] = $this->Global_model->get_user_all();

		$data['page'] 			= "data_pinjaman";
		$data['judul'] 			= "Master";
		$data['deskripsi'] 		= "users";

		$this->template->views('master/list_user', $data);
	}

	public function data_pinjaman()
	{

		$data['userdata'] 		= $this->userdata;

		$data['page'] 			= "data_pinjaman";
		$data['judul'] 			= "Master";
		$data['deskripsi'] 		= "Data Pinjaman";

		$data['pinjaman'] = $this->Global_model->get_pinjaman_all();

		$this->template->views('master/list_pinjaman', $data);
	}

	public function tambah_pinjaman()
	{

		$data['userdata'] 		= $this->userdata;

		$data['nasabah'] 		= $this->Global_model->get_nasabah_non_pinjaman_all();

		$data['page'] 			= "data_pinjaman";
		$data['judul'] 			= "Tambah Pinjaman";
		$data['deskripsi'] 		= "";

		$this->template->views('master/form_pinjaman', $data);
	}

	public function edit_pinjaman($pinjaman_id)
	{

		$data['userdata'] 		= $this->userdata;

		$data['pinjaman'] = $this->Global_model->get_pinjaman_by_id($pinjaman_id);

		$data['nasabah'] = $this->Global_model->get_nasabah_all();

		$data['page'] 			= "data_pinjaman";
		$data['judul'] 			= "Edit Pinjaman";
		$data['deskripsi'] 		= "";

		$this->template->views('master/form_pinjaman', $data);
	}

	public function edit_user($user_id)
	{

		$data['userdata'] 		= $this->userdata;

		$data['user'] = $this->Global_model->get_user_by_id($user_id);

		$data['page'] 			= "data_users";
		$data['judul'] 			= "Edit User";
		$data['deskripsi'] 		= "";

		$this->template->views('master/form_user', $data);
	}

	public function insert_pinjaman()
	{

		$data['userdata'] 		= $this->userdata;
		$data = $this->input->post();
		$pinjaman_id = $this->input->post('pinjaman_id', TRUE);
		unset($data['pinjaman_id']);

		$flag = $this->input->post('flag', TRUE);

		if ($flag == 'flag') {

			if (!empty($flag)) {
				if (!empty($pinjaman_id)) {

					unset($data['flag']);
					$result = $this->Global_model->update_pinjaman($data, $pinjaman_id);

					if ($result == true) {
						$this->session->set_flashdata('msg', show_succ_msg('Pinjaman Berhasil Di Update'));
						$url = base_url() . 'master/data_pinjaman';
						redirect($url);
						header("Refresh:0");
					} else {
						$this->session->set_flashdata('msg', show_err_msg('Pinjaman Gagal Di Update'));
						$url = base_url() . 'master/data_pinjaman';
						redirect($url);
						header("Refresh:0");
					}
				} else {

					unset($data['flag']);
					$result = $this->Global_model->insert_pinjaman($data);

					if ($result == true) {
						$this->session->set_flashdata('msg', show_succ_msg('Pinjaman Berhasil Dimasukkan'));
						$url = base_url() . 'master/data_pinjaman';
						redirect($url);
						header("Refresh:0");
					} else {
						$this->session->set_flashdata('msg', show_err_msg('Pinjaman Gagal Dimasukkan'));
						$url = base_url() . 'master/data_pinjaman/';
						redirect($url);
						header("Refresh:0");
					}
				}
			}
		}

		$this->template->views('master/form_pinjaman', $data);
	}

	public function insert_user()
	{

		$data['userdata'] 		= $this->userdata;
		$data = $this->input->post();
		$user_id = $this->input->post('user_id', TRUE);
		unset($data['user_id']);

		$flag = $this->input->post('flag', TRUE);

		if ($flag == 'flag') {

			if (!empty($flag)) {
				if (!empty($user_id)) {

					unset($data['flag']);
					$result = $this->Global_model->update_user($data, $user_id);

					if ($result == true) {
						$this->session->set_flashdata('msg', show_succ_msg('User Berhasil Di Update'));
						$url = base_url() . 'master/data_users';
						redirect($url);
						header("Refresh:0");
					} else {
						$this->session->set_flashdata('msg', show_err_msg('User Gagal Di Update'));
						$url = base_url() . 'master/data_users';
						redirect($url);
						header("Refresh:0");
					}
				} else {

					unset($data['flag']);
					$result = $this->Global_model->insert_user($data);

					if ($result == true) {
						$this->session->set_flashdata('msg', show_succ_msg('User Berhasil Dimasukkan'));
						$url = base_url() . 'master/data_users';
						redirect($url);
						header("Refresh:0");
					} else {
						$this->session->set_flashdata('msg', show_err_msg('User Gagal Dimasukkan'));
						$url = base_url() . 'master/data_users';
						redirect($url);
						header("Refresh:0");
					}
				}
			}
		}

		$this->template->views('master/form_pinjaman', $data);
	}

	public function tambah_user()
	{

		$data['userdata'] 		= $this->userdata;

		$data['page'] 			= "data_users";
		$data['judul'] 			= "Tambah User";
		$data['deskripsi'] 		= "";

		$this->template->views('master/form_user', $data);
	}	

	public function delete_pinjaman()
	{

		$pinjaman_id = $this->input->post('id');

		$result = $this->Global_model->delete_pinjaman($pinjaman_id);
		
		if ($result == true) {
			$this->session->set_flashdata('msg', show_succ_msg('Pinjaman Berhasil di Hapus'));
			redirect('master/data_pinjaman');
		} else {
			$this->session->set_flashdata('msg', show_err_msg('Pinjaman Gagal Di Hapus'));
			redirect('master/data_pinjaman');
		}
	}

	public function delete_user()
	{

		$user_id = $this->input->post('id');

		$result = $this->Global_model->delete_user($user_id);

		if ($result == true) {
			$this->session->set_flashdata('msg', show_succ_msg('User Berhasil Hapus'));
			redirect('master/data_users');
		} else {
			$this->session->set_flashdata('msg', show_err_msg('User Gagal Dihapus'));
			redirect('master/data_users');
		}
	}

	public function change_password()
	{
		$this->form_validation->set_rules('passLama', 'Password Lama', 'trim|required');
		$this->form_validation->set_rules('passBaru', 'Password Baru', 'trim|required');
		$this->form_validation->set_rules('passKonf', 'Password Konfirmasi', 'trim|required');

		$UserID = $this->userdata->UserID;

		if ($this->form_validation->run() == TRUE) {
			if (($this->input->post('passLama')) == $this->userdata->Passwd) {
				if ($this->input->post('passBaru') != $this->input->post('passKonf')) {
					$this->session->set_flashdata('msg', show_err_msg('Password Baru dan Konfirmasi Password harus sama'));
					redirect('profile');
				} else {
					$data = [
						'Passwd' => $this->input->post('passBaru')
					];

					$result = $this->Global_model->update($data, $UserID);

					if ($result > 0) {
						$this->session->set_flashdata('msg', show_succ_msg('Password Berhasil diubah'));
						redirect('profile');
					} else {
						$this->session->set_flashdata('msg', show_err_msg('Password Gagal diubah'));
						redirect('profile');
					}
				}
			} else {
				$this->session->set_flashdata('msg', show_err_msg('Password Salah'));
				redirect('profile');
			}
		} else {
			$this->session->set_flashdata('msg', show_err_msg(validation_errors()));
			redirect('profile');
		}
	}
}
