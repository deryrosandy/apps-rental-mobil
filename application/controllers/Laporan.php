<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Laporan extends AUTH_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model('Global_model');
	}

	public function index() {
		$data['userdata'] = $this->userdata;
		$data['page'] = "report";
		$data['judul'] = "Report";
		$data['deskripsi'] = "Laporan Harian";
		$this->template->views('report/report_harian', $data);
	}
	
	public function data_restrukturisasi() {
		$data['userdata'] = $this->userdata;
		$data['page'] = "laporan_pengajuan";
		$data['judul'] = "Laporan Data Restrukturisasi";
		$data['deskripsi'] = "";

		$data['data_pengajuan'] = $this->Global_model->get_pengajuan_all();

		$this->template->views('laporan/data_restrukturisasi', $data);
	}	
	public function data_nasabah() {
		$data['userdata'] = $this->userdata;
		$data['page'] = "laporan_nasabah";
		$data['judul'] = "Laporan Data Nasabah";
		$data['deskripsi'] = "";

		$data['data_nasabah'] = $this->Global_model->get_nasabah_all();

		$this->template->views('laporan/data_nasabah', $data);
	}	
	public function data_pinjaman() {
		$data['userdata'] = $this->userdata;
		$data['page'] = "laporan_pinjaman";
		$data['judul'] = "Laporan Data Pinjaman";
		$data['deskripsi'] = "";

		$data['data_pinjaman'] = $this->Global_model->get_pinjaman_all();

		$this->template->views('laporan/data_pinjaman', $data);
	}	

	public function print_data_pinjaman() {

		$data['userdata'] = $this->userdata;
		$data['page'] = "laporan";
		$data['judul'] = "Laporan Data Pinjaman";
		$data['deskripsi'] = "";

		$data['data_pinjaman'] = $this->Global_model->get_pinjaman_all();

		$this->pdf->load_view('laporan/print_data_pinjaman', $data);
		$this->pdf->set_paper('A4', 'portrait');
		$this->pdf->render();
		$this->pdf->stream("cetak_data_pinjaman.pdf", array("Attachment" => false));		
	}		

	public function print_data_nasabah() {

		$data['userdata'] = $this->userdata;
		$data['page'] = "laporan";
		$data['judul'] = "Laporan Data Nasabah";
		$data['deskripsi'] = "";

		$data['data_nasabah'] = $this->Global_model->get_nasabah_all();

		$this->pdf->load_view('laporan/print_data_nasabah', $data);
		$this->pdf->set_paper('A4', 'portrait');
		$this->pdf->render();
		$this->pdf->stream("cetak_data_nasabah.pdf", array("Attachment" => false));		
	}		

	public function cetak_laporan_restrukturisasi() {
		$dari_tanggal = $this->input->get('dari_tanggal');
		$sampai_tanggal = $this->input->get('sampai_tanggal');

		$data['data_pengajuan'] = $this->Global_model->get_pengajuan_by_date_range($dari_tanggal, $sampai_tanggal);

		$this->pdf->load_view('laporan/print_data_restrukturisasi', $data);
		$this->pdf->set_paper('A4', 'portrait');
		$this->pdf->render();
		$this->pdf->stream("cetak_restrukturisasi.pdf", array("Attachment" => false));		
	}	

}