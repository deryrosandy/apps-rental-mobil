<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Nasabah extends AUTH_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model(array('Global_model'));
	}

	public function index()
	{

		$this->data_nasabah();
	}

	public function data_nasabah()
	{

		$data['userdata'] 		= $this->userdata;

		$data['nasabah'] = $this->Global_model->get_nasabah_all();

		$data['page'] 			= "data_nasabah";
		$data['judul'] 			= "Nasabah";
		$data['deskripsi'] 		= "List Data";

		$this->template->views('nasabah/list_nasabah', $data);
	}

	public function tambah_nasabah()
	{

		$data['userdata'] 		= $this->userdata;

		$data['page'] 			= "tambah_nasabah";
		$data['judul'] 			= "Nasabah";
		$data['deskripsi'] 		= "Tambah Nasabah";

		$data['pinjaman'] = $this->Global_model->get_pinjaman_all();

		$this->template->views('nasabah/form_nasabah', $data);
	}

	public function edit_nasabah($nasabah_id){

		$data['userdata'] 		= $this->userdata;	

		$data['nasabah'] = $this->Global_model->get_nasabah_by_nasabah_id($nasabah_id);

		$data['page'] 			= "edit_nasabah";
		$data['judul'] 			= "Edit Nasabah";
		$data['deskripsi'] 		= "";
		
		$this->template->views('nasabah/form_nasabah', $data);
	
	}

	public function insert_nasabah()
	{

		$data['userdata'] 		= $this->userdata;
		$data = $this->input->post();
		$nasabah_id = $this->input->post('nasabah_id', TRUE);
		unset($data['nasabah_id']);

		$flag = $this->input->post('flag', TRUE);

		if ($flag == 'flag') {

			if (!empty($flag)) {
				if (!empty($nasabah_id)) {

					unset($data['flag']);

					$result = $this->Global_model->update_nasabah($data, $nasabah_id);

					if ($result == true) {
						$this->session->set_flashdata('msg', show_succ_msg('Nasabah Berhasil Di Update'));
						$url = base_url() . 'nasabah/index';
						redirect($url);
						header("Refresh:0");
					} else {
						$this->session->set_flashdata('msg', show_err_msg('Nasabah Gagal Di Update'));
						$url = base_url() . 'nasabah/index';
						redirect($url);
						header("Refresh:0");
					}
				} else {

					unset($data['flag']);

					$result = $this->Global_model->insert_nasabah($data);

					if ($result == true) {
						$this->session->set_flashdata('msg', show_succ_msg('Nasabah Berhasil Dimasukkan'));
						$url = base_url() . 'nasabah/index';
						redirect($url);
						header("Refresh:0");
					} else {
						$this->session->set_flashdata('msg', show_err_msg('Nasabah Gagal Dimasukkan'));
						$url = base_url() . 'nasabah/index';
						redirect($url);
						header("Refresh:0");
					}
				}
			}
		}

		$this->template->views('nasabah/form_nasabah', $data);
	}

	public function delete_nasabah()
	{

		$nasabah_id = $this->input->post('id');

		$result = $this->Global_model->delete_nasabah($nasabah_id);

		if ($result == true) {
			$this->session->set_flashdata('msg', show_succ_msg('Nasabah Berhasil di Hapus'));
			redirect('nasabah/index');
		} else {
			$this->session->set_flashdata('msg', show_err_msg('Nasabah Gagal Di Hapus'));
			redirect('nasabah/index');
		}
	}
}
