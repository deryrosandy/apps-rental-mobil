<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Restrukturisasi extends AUTH_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model(array('Global_model'));
	}
	
	public function index() {
		$this->data_restrukturisasi();
	}

	public function tambah_data() {

		$data['userdata'] 		= $this->userdata;	
		$data['page'] 			= "tambah_data";
		$data['judul'] 			= "Restrukturisasi ";
		$data['deskripsi'] 		= "";

		$data['data_nasabah'] = $this->Global_model->get_nasabah_all();

		$this->template->views('restrukturisasi/form_restrukturisasi', $data);
	}

	public function create_new() {

		$data['userdata'] 		= $this->userdata;	
		$data['page'] 			= "tambah_data";
		$data['judul'] 			= "Restrukturisasi ";
		$data['deskripsi'] 		= "";

		$nasabah = $this->Global_model->get_nasabah_by_user_id($data['userdata']->user_id);
		$data['data_pinjaman'] = $this->Global_model->get_pinjaman_nasabah_by_nasabah_id($nasabah->nasabah_id);

		$data['pengajuan'] = $this->Global_model->get_pengajuan_all_by_nasabah_id($nasabah->nasabah_id);

		$this->template->views('restrukturisasi/form_restrukturisasi_nasabah', $data);
	}

	public function delete()
	{

		$id = $this->input->post('id');

		$result = $this->Global_model->delete_pengajuan($id);

		if ($result == true) {
			$this->session->set_flashdata('msg', show_succ_msg('Data Berhasil Hapus'));
			redirect('restrukturisasi/data_restrukturisasi');
		} else {
			$this->session->set_flashdata('msg', show_err_msg('Data Gagal Dihapus'));
			redirect('restrukturisasi/data_restrukturisasi');
		}
	}

	public function print_restrukturisasi($pengajuan_id) {
		
		$data['pengajuan'] = $this->Global_model->get_detail_pengajuan_by_id($pengajuan_id);
		
		$this->pdf->load_view('restrukturisasi/print_restrukturisasi', $data);
		$this->pdf->set_paper('A4', 'portrait');
		$this->pdf->render();
		$this->pdf->stream("print_restrukturisasi_" . str_replace("-","", $pengajuan_id) . ".pdf", array("Attachment" => false));		

	}

	public function submit_pengajuan() {
		
		$userdata 		= $this->userdata;
		$data_post 	= $this->input->post();

		$nasabah_id = $data_post['nasabah_id'];
		$data_pinjaman = $this->Global_model->get_pinjaman_nasabah_by_nasabah_id($nasabah_id);
		$data_nasabah = $this->Global_model->get_nasabah_by_nasabah_id($nasabah_id);

		$data = array(
			"nasabah_id" => $nasabah_id,
			"pinjaman_id" => $data_pinjaman->pinjaman_id,
			"nama_nasabah" => $data_nasabah->nama_lengkap,
			"no_telp" => $data_nasabah->notelp,
			"no_rek" => $data_nasabah->norek,
			"jml_pinjaman" => $data_pinjaman->jml_pinjaman,
			"tenor" => $data_pinjaman->tenor,
			"lama_penangguhan" => $data_post['lama_penangguhan'],
			"alamat" => $data_nasabah->alamat,
			"keterangan" => $data_post['keterangan'],
			"status" => 'open',
			"created_at" => date('Y-m-d H:i:s'),
		);
		
		$result = $this->Global_model->insert_pengajuan($data);
		
		if ($result == true) {
			$this->session->set_flashdata('msg', show_succ_msg('Data Berhasil di Masukkan'));
			redirect('restrukturisasi/data_restrukturisasi');
		} else {
			$this->session->set_flashdata('msg', show_err_msg('Data Gagal Di Masukkan'));
			redirect('restrukturisasi/data_restrukturisasi');
		}	
	}

	public function submit_pengajuan_nasabah() {
		
		$userdata 		= $this->userdata;
		$data_post 	= $this->input->post();

		$nasabah_id = $data_post['nasabah_id'];
		$data_pinjaman = $this->Global_model->get_pinjaman_nasabah_by_nasabah_id($nasabah_id);
		$data_nasabah = $this->Global_model->get_nasabah_by_nasabah_id($nasabah_id);
		
		$data = array(
			"nasabah_id" => $nasabah_id,
			"pinjaman_id" => $data_pinjaman->pinjaman_id,
			"nama_nasabah" => $data_nasabah->nama_lengkap,
			"no_telp" => $data_nasabah->notelp,
			"no_rek" => $data_nasabah->norek,
			"jml_pinjaman" => $data_pinjaman->jml_pinjaman,
			"tenor" => $data_pinjaman->tenor,
			"lama_penangguhan" => $data_post['lama_penangguhan'],
			"alamat" => $data_nasabah->alamat,
			"keterangan" => $data_post['keterangan'],
			"status" => 'open',
			"created_at" => date('Y-m-d H:i:s'),
		);
		
		$result = $this->Global_model->insert_pengajuan($data);
		
		if ($result == true) {
			$this->session->set_flashdata('msg', show_succ_msg('Data Berhasil di Masukkan'));
			redirect('restrukturisasi/my_data');
		} else {
			$this->session->set_flashdata('msg', show_err_msg('Data Gagal Di Masukkan'));
			redirect('restrukturisasi/my_data');
		}	
	}

	public function data_restrukturisasi() {
		
		$data['userdata'] 		= $this->userdata;	
		$data['page'] 			= "data_restrukturisasi";
		$data['judul'] 			= "Data Restrukturisasi";
		$data['deskripsi'] 		= "";

		$data['data_pengajuan'] = $this->Global_model->get_pengajuan_all();

		$this->template->views('restrukturisasi/list_data_all', $data);
	}

	public function my_data() {
		
		$data['userdata'] 		= $this->userdata;	
		$data['page'] 			= "data_restrukturisasi";
		$data['judul'] 			= "Data Restrukturisasi";
		$data['deskripsi'] 		= "";

		$nasabah = $this->Global_model->get_nasabah_by_user_id($data['userdata']->user_id);
		$data['data_pengajuan'] = $this->Global_model->get_pengajuan_all_by_nasabah_id($nasabah->nasabah_id);

		$this->template->views('restrukturisasi/list_data_all', $data);
	}

	public function data_open() {
		
		$data['userdata'] 		= $this->userdata;	
		$data['page'] 			= "data_open";
		$data['judul'] 			= "Data Restrukturisasi";
		$data['deskripsi'] 		= "";

		$data['data_pengajuan'] = $this->Global_model->get_pengajuan_open();

		$this->template->views('restrukturisasi/list_data_all', $data);
	}

	public function data_approved() {
		
		$data['userdata'] 		= $this->userdata;	
		$data['page'] 			= "data_proses";
		$data['judul'] 			= "Data Restrukturisasi";
		$data['deskripsi'] 		= "";

		$data['data_pengajuan'] = $this->Global_model->get_pengajuan_approved();

		$this->template->views('restrukturisasi/list_data_all', $data);
	}

	public function data_reject() {
		
		$data['userdata'] 		= $this->userdata;	
		$data['page'] 			= "data_ditolak";
		$data['judul'] 			= "Data Restrukturisasi";
		$data['deskripsi'] 		= "";

		$data['data_pengajuan'] = $this->Global_model->get_pengajuan_reject();

		$this->template->views('restrukturisasi/list_data_all', $data);
	}

	public function detail($pengajuan_id) {
		
		$data['userdata'] 		= $this->userdata;	
		$data['page'] 			= "data_restrukturisasi";
		$data['judul'] 			= "Data Restrukturisasi";
		$data['deskripsi'] 		= "";

		$data['data_pengajuan'] = $this->Global_model->get_detail_pengajuan_by_id($pengajuan_id);

		$this->template->views('restrukturisasi/detail', $data);
	}

	public function get_pinjaman_nasabah() {
		
		$userdata 		= $this->userdata;
		$nasabah_id 	= $this->input->post('nasabah_id');

		$data_pinjaman = $this->Global_model->get_pinjaman_nasabah_by_nasabah_id($nasabah_id);

		echo json_encode($data_pinjaman);

	}

	public function approve()
	{

		$id = $this->input->post('id');

		$result = $this->Global_model->approve_pengajuan($id);

		if ($result == true) {
			$this->session->set_flashdata('msg', show_succ_msg('Berhasil Di Approve'));
			redirect('restrukturisasi/detail/' . $id);
		} else {
			$this->session->set_flashdata('msg', show_err_msg('Gagal Di Approve'));
			redirect('restrukturisasi/detail/' . $id);
		}
	}

	public function reject()
	{

		$id = $this->input->post('id');

		$result = $this->Global_model->reject_pengajuan($id);

		if ($result == true) {
			$this->session->set_flashdata('msg', show_succ_msg('Berhasil Di Reject'));
			redirect('restrukturisasi/detail/' . $id);
		} else {
			$this->session->set_flashdata('msg', show_err_msg('Gagal Di Reject'));
			redirect('restrukturisasi/detail/' . $id);
		}
	}

}