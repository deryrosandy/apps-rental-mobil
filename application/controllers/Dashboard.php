<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Dashboard extends AUTH_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Global_model');
	}

	public function index()
	{

		$data['userdata'] 		= $this->userdata;

		$data['page'] 			= "dashboard";
		$data['judul'] 			= "Dashboard";
		$data['deskripsi'] 		= "";

		$date_now = date('Y-m-d');

		$user_type = $data['userdata']->user_type;
	
		if ($data['userdata']->user_type == 'admin') {

			$data['total_nasabah'] 	= $this->Global_model->get_total_nasabah();
			$data['total_pengajuan'] 	= $this->Global_model->get_total_pengajuan();
			$data['total_cicilan'] 	= $this->Global_model->get_total_pinjaman();
			$data['total_user'] 	= $this->Global_model->get_total_user($user_type);

			$data['page'] 	= 'dashboard';
			$data['deskripsi'] 	= 'Dashboard';
			$data['judul'] = "Restrukturisasi Cicilan";

			$this->template->views('dashboard', $data);

		} elseif ($data['userdata']->user_type == 'nasabah') { 

			$data['userdata'] 		= $this->userdata;	
			$data['page'] 			= "dashboard";
			$data['judul'] 			= "Informasi ";
			$data['deskripsi'] 		= "";

			$nasabah = $this->Global_model->get_nasabah_by_user_id($data['userdata']->user_id);
			$data['data_pinjaman'] = $this->Global_model->get_pinjaman_nasabah_by_nasabah_id($nasabah->nasabah_id);

			$data['pengajuan'] = $this->Global_model->get_pengajuan_all_by_nasabah_id($nasabah->nasabah_id);
			
			$this->template->views('dashboard', $data);
		}
	}
}
