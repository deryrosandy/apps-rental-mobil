<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model('Global_model');
	}
	
	public function index() {
		$session = $this->session->userdata('status');
		
		if ($session == '') {
			$this->load->view('login');
		} else {
			redirect('dashboard');
		}
	}

	public function login() {
		$this->form_validation->set_rules('username', 'Username', 'required|min_length[3]|max_length[15]');
		$this->form_validation->set_rules('password', 'Password', 'required');

		if ($this->form_validation->run() == TRUE) {
			$username = trim($_POST['username']);
			$password = md5(trim($_POST['password']));
			$data = $this->Global_model->login($username, $password);
			

			if ($data == false) {
				$this->session->set_flashdata('error_msg', 'Username / Password Anda Salah.');
				redirect('auth');
			} else {
				$session = [
					'userdata' => $data,
					'status' => "Loged in"
				];
				$this->session->set_userdata($session);
				redirect('dashboard');
			}
		} else {
			$this->session->set_flashdata('error_msg', validation_errors());
			redirect('auth');
		}
	}

	public function logout() {
		$this->session->sess_destroy();
		redirect('auth');
	}
}

/* End of file Login.php */
/* Location: ./application/controllers/Login.php */