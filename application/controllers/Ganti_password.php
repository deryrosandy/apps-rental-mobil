<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Ganti_password extends AUTH_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Global_model');
	}

	public function index()
	{
		$data['userdata'] 		= $this->userdata;

		$data['page'] 			= "change_password";
		$data['judul'] 			= "User Profile";
		$data['deskripsi'] 		= "Ganti Password";

		$this->template->views('change_password', $data);
	}

	public function update()
	{
		$user_id = $this->userdata->user_id;

		$data = $this->input->post();
		//var_dump($data); die();
		if ($this->form_validation->run() == TRUE) {

			$result = $this->Global_model->update_profile($data, $user_id);

			if ($result > 0) {
				$this->updateProfil();
				$this->session->set_flashdata('msg', show_succ_msg('Data profile Berhasil diubah'));
				redirect('profile');
			} else {
				$this->session->set_flashdata('msg', show_err_msg('Data profile Gagal diubah'));
				redirect('profile');
			}
		} else {
			$this->session->set_flashdata('msg', show_err_msg(validation_errors()));
			redirect('profile');
		}
	}

	public function change_password()
	{
		$this->form_validation->set_rules('passLama', 'Password Lama', 'trim|required');
		$this->form_validation->set_rules('passBaru', 'Password Baru', 'trim|required');
		$this->form_validation->set_rules('passKonf', 'Password Konfirmasi', 'trim|required');
		
		$UserID = $this->userdata->user_id;

		if ($this->form_validation->run() == TRUE) {
			if (md5($this->input->post('passLama')) == $this->userdata->password) {
				if ($this->input->post('passBaru') != $this->input->post('passKonf')) {
					$this->session->set_flashdata('msg', show_err_msg('Password Baru dan Konfirmasi Password harus sama'));
					redirect('ganti_password');
				} else {
					$data = [
						'password' => md5($this->input->post('passBaru'))
					];

					$result = $this->Global_model->update_password($data, $UserID);

					if ($result > 0) {
						$this->session->set_flashdata('msg', show_succ_msg('Password Berhasil diubah'));
						redirect('ganti_password');
					} else {
						$this->session->set_flashdata('msg', show_err_msg('Password Gagal diubah'));
						redirect('ganti_password');
					}
				}
			} else {
				$this->session->set_flashdata('msg', show_err_msg('Password Salah'));
				redirect('ganti_password');
			}
		} else {
			$this->session->set_flashdata('msg', show_err_msg(validation_errors()));
			redirect('ganti_password');
		}
	}
}
