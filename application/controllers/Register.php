<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Register extends CI_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model(array('Global_model'));
	}

	public function index() {
		
		$data['page'] 			= "register";
		$data['judul'] 			= "Registrasi Pengajuan Restrukturisasi Cicilan BRI";
		$data['deskripsi'] 		= "";

		$this->load->view('register', $data);
	}

	public function submit_register() {
		
		$data 	= $this->input->post();		
		
		$result = $this->Global_model->submit_register($data);
		
		if ($result == true) {
			$this->session->set_flashdata('msg', show_succ_msg('Registrasi Berhasil'));
			redirect('auth');
		} else {
			$this->session->set_flashdata('msg', show_err_msg('Registrasi Gagal'));
			redirect('auth');
		}		
	}

	public function cek_nasabah()
	{

		$no_ktp = $this->input->post('no_ktp');
		$no_rek = $this->input->post('no_rek');

		$result = $this->Global_model->cek_nasabah_by_ktp_norek($no_ktp, $no_rek);
		echo json_encode($result);
	}

}