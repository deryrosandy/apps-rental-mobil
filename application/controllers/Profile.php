<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class profile extends AUTH_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model('Global_model');
	}

	public function index() {
		$data['userdata'] 		= $this->userdata;		
		$data['page'] 			= "profile";
		$data['judul'] 			= "User Profile";
		$data['deskripsi'] 		= "Detail";
		$this->template->views('profile', $data);
	}

	public function update() {
		$this->form_validation->set_rules('nama_lengkap', 'nama_lengkap', 'trim|required');

		$UserID = $this->userdata->user_id;	
		
		$data = $this->input->post();

		if ($this->form_validation->run() == TRUE) {
			
			$result = $this->Global_model->update_profile($data, $UserID);
			
			if ($result > 0) {
				//$this->updateProfil();
				$this->session->set_flashdata('msg', show_succ_msg('Data profile Berhasil diubah'));
				//$userdata->username
				redirect('profile');
			} else {
				$this->session->set_flashdata('msg', show_err_msg('Data profile Gagal diubah'));
				redirect('profile');
			}
		} else {
			$this->session->set_flashdata('msg', show_err_msg(validation_errors()));
			redirect('profile');
		}
	}

	public function change_password() {
		$this->form_validation->set_rules('passLama', 'Password Lama', 'trim|required');
		$this->form_validation->set_rules('passBaru', 'Password Baru', 'trim|required');
		$this->form_validation->set_rules('passKonf', 'Password Konfirmasi', 'trim|required');
		
		$UserID = $this->userdata->id_user;
	
		if ($this->form_validation->run() == TRUE) {
			$pass_lama = $this->Global_model->select($UserID)->password;
			
			if (md5($this->input->post('passLama')) == $pass_lama) {
				
				if ($this->input->post('passBaru') != $this->input->post('passKonf')) {
					$this->session->set_flashdata('msg', show_err_msg('Password Baru dan Konfirmasi Password harus sama'));
					redirect('profile');
				} else {
					$data = [
						'password' => md5($this->input->post('passBaru'))
					];
					
					$result = $this->Global_model->update($data, $UserID);
					
					if ($result > 0) {
						$this->session->set_flashdata('msg', show_succ_msg('Password Berhasil diubah'));
						redirect('profile');
					} else {
						$this->session->set_flashdata('msg', show_err_msg('Password Gagal diubah'));
						redirect('profile');
					}
				}
			} else {
				$this->session->set_flashdata('msg', show_err_msg('Password Salah'));
				redirect('profile');
			}
		} else {
			$this->session->set_flashdata('msg', show_err_msg(validation_errors()));
			redirect('profile');
		}
	}


}

/* End of file profile.php */
/* Location: ./application/controllers/profile.php */