<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Global_model extends CI_Model
{

	public function update_password($data, $UserID)
	{

		$this->db->where('user_id', $UserID);
		$this->db->update('users', $data);

		return true;
	}

	public function update_profile($data, $UserID)
	{

		$this->db->where('user_id', $UserID);
		$this->db->update('users', $data);

		return true;
	}

	public function select($UserID = '')
	{
		if ($UserID != '') {
			$this->db->where('user_id', $UserID);
		}

		$data = $this->db->get('users');

		return $data->row();
	}

	public function login($user, $pass)
	{
		$this->db->select('*');
		$this->db->from('users');
		$this->db->where('username', $user);
		$this->db->where('password', $pass);
		$this->db->where('status', 1);

		$data = $this->db->get();

		if ($data->num_rows() == 1) {
			return $data->row();
		} else {
			return false;
		}
	}

	public function get_user_by_id($user_id)
	{
		//var_dump($user_id); die();
		$sql = "SELECT * FROM users Where user_id='" . $user_id . "'";

		$data = $this->db->query($sql);

		return $data->row();
	}


	public function get_nasabah_by_user_id($user_id)
	{
		//var_dump($user_id); die();
		$sql = "SELECT * FROM nasabah Where user_id='" . $user_id . "'";

		$data = $this->db->query($sql);

		return $data->row();
	}

	public function get_detail_pengajuan_by_id($pengajuan_id)
	{
		$this->db->select('a.*, a.status as status_pengajuan, c.*');
		$this->db->select('b.nama_lengkap, b.norek, b.notelp');
		$this->db->from('pengajuan a');
		$this->db->join("nasabah b", "b.nasabah_id = a.nasabah_id");
		$this->db->join("pinjaman c", "c.pinjaman_id = a.pinjaman_id");
		$this->db->where("a.pengajuan_id", $pengajuan_id);
		$query = $this->db->get();

		return $query->row();
	}

	public function get_nasabah_by_nasabah_id($nasabah_id)
	{
		//var_dump($user_id); die();
		$sql = "SELECT * FROM nasabah Where nasabah_id='" . $nasabah_id . "'";

		$data = $this->db->query($sql);

		return $data->row();
	}

	public function cek_nasabah_by_ktp_norek($no_ktp, $no_rek)
	{
		$this->db->select('*');
		$this->db->from('nasabah');
		$this->db->where("noktp", $no_ktp);
		$this->db->where("norek", $no_rek);

		$query = $this->db->get();

		if ($query->num_rows() > 0) {
			return $query->row();
		} else {
			return false;
		}
	}

	public function get_total_nasabah()
	{
		$sql = "SELECT * FROM nasabah";

		$data = $this->db->query($sql);

		return $data->num_rows();
	}

	public function get_total_pinjaman()
	{

		$sql = "SELECT * FROM pinjaman";
		$data = $this->db->query($sql);

		return $data->num_rows();
	}

	public function get_total_pengajuan()
	{
		$sql = "SELECT * FROM pengajuan";

		$data = $this->db->query($sql);

		return $data->num_rows();
	}

	public function get_total_pengajuan_by_nasabah_id($nasabah_id)
	{
		$sql = "SELECT * FROM pengajuan Where nasabah_id='$nasabah_id'";

		$data = $this->db->query($sql);

		return $data->num_rows();
	}

	public function insert_pinjaman($data)
	{
		$data['created_at'] = date('Y-m-d H:i:s');
		$result = $this->db->insert('pinjaman', $data);

		if ($result == true) {
			return true;
		} else {
			return false;
		}
	}
	public function insert_pengajuan($data)
	{
		$result = $this->db->insert('pengajuan', $data);

		if ($result == true) {
			return true;
		} else {
			return false;
		}
	}
	public function submit_register($data)
	{		
		$nasabah_id = $data['nasabah_id'];
		$data_nasabah = $this->get_nasabah_by_nasabah_id($nasabah_id);
		
		$data_user = array(
			"username" 	=> $data['username'],
			"email" 	=> $data['email'],
			"nama_lengkap" => $data_nasabah->nama_lengkap,
			"user_type" => 'nasabah',
			"status" => 1,
			"password" => md5($data['password']),
			"created_at" => date('Y-m-d H:i:s')
		);

		$result = $this->db->insert('users', $data_user);
		$user_id = $this->db->insert_id();

		if ($result == true) {
			$this->db->update('nasabah', array("user_id"=>$user_id), array("nasabah_id"=>$nasabah_id));

			return 'User Berhasil Di Masukkan';
		} else {
			return 'User Gagal Di Masukkan';
		}

		if ($result == true) {
			return true;
		} else {
			return false;
		}
	}

	public function get_total_user()
	{
		$sql = "SELECT * FROM users";

		$data = $this->db->query($sql);

		return $data->num_rows();
	}

	public function get_pinjaman_all()
	{

		$this->db->select('a.*');
		$this->db->select('b.nama_lengkap, b.norek, b.notelp');
		$this->db->from('pinjaman a');
		$this->db->join("nasabah b", "b.nasabah_id = a.nasabah_id");

		$query = $this->db->get();

		return $query->result();
	}

	public function get_nasabah_non_pinjaman_all()
	{

		$this->db->select('*');
		$this->db->from('nasabah');
		$this->db->where('nasabah_id NOT IN (select nasabah_id from pinjaman)');
		$this->db->group_by("nasabah_id");

		$query = $this->db->get();

		return $query->result();
	}

	public function get_nasabah_all()
	{

		$sql = "SELECT * FROM nasabah";

		$query = $this->db->query($sql);

		return $query->result();
	}
	public function get_user_all()
	{

		$sql = "SELECT * FROM users";

		$query = $this->db->query($sql);

		return $query->result();
	}
	public function get_pengajuan_all()
	{

		$this->db->select('a.*');
		$this->db->select('b.nama_lengkap, b.norek, b.notelp');
		$this->db->from('pengajuan a');
		$this->db->join("nasabah b", "b.nasabah_id = a.nasabah_id");
		$query = $this->db->get();

		return $query->result();
	}
	public function get_pengajuan_open()
	{

		$this->db->select('a.*');
		$this->db->select('b.nama_lengkap, b.norek, b.notelp');
		$this->db->from('pengajuan a');
		$this->db->join("nasabah b", "b.nasabah_id = a.nasabah_id");
		$this->db->where("a.status" , "open");
		$query = $this->db->get();

		return $query->result();
	}
	public function get_pengajuan_approved()
	{

		$this->db->select('a.*');
		$this->db->select('b.nama_lengkap, b.norek, b.notelp');
		$this->db->from('pengajuan a');
		$this->db->join("nasabah b", "b.nasabah_id = a.nasabah_id");
		$this->db->where("a.status" , "approved");
		$query = $this->db->get();

		return $query->result();
	}
	public function get_pengajuan_all_by_nasabah_id($nasabah_id)
	{

		$this->db->select('a.*');
		$this->db->select('b.nama_lengkap, b.norek, b.notelp');
		$this->db->from('pengajuan a');
		$this->db->join("nasabah b", "b.nasabah_id = a.nasabah_id");
		$this->db->where("a.nasabah_id" , $nasabah_id);
		$query = $this->db->get();

		return $query->result();
	}
	public function get_pengajuan_reject()
	{

		$this->db->select('a.*');
		$this->db->select('b.nama_lengkap, b.norek, b.notelp');
		$this->db->from('pengajuan a');
		$this->db->join("nasabah b", "b.nasabah_id = a.nasabah_id");
		$this->db->where("a.status" , "reject");
		$query = $this->db->get();

		return $query->result();
	}
	public function get_pengajuan_by_date_range($dari_tanggal, $sampai_tanggal)
	{

		$this->db->select('a.*');
		$this->db->select('b.nama_lengkap, b.norek, b.notelp');
		$this->db->from('pengajuan a');
		$this->db->join("nasabah b", "b.nasabah_id = a.nasabah_id");
		$this->db->where("a.created_at >= '$dari_tanggal'");
		$this->db->where("a.created_at <= '$sampai_tanggal'");
		$query = $this->db->get();

		return $query->result();
	}

	public function get_pinjaman_by_id($pinjaman_id)
	{
		$sql = "SELECT * FROM pinjaman WHERE pinjaman_id = '" . $pinjaman_id . "'";

		$query = $this->db->query($sql);

		return $query->row();
	}

	public function get_pinjaman_nasabah_by_nasabah_id($nasabah_id)
	{
		$this->db->select('a.*');
		$this->db->select('b.nama_lengkap, b.alamat, b.norek as no_rek, b.notelp');
		$this->db->from('pinjaman a');
		$this->db->join("nasabah b", "b.nasabah_id = a.nasabah_id");
		$this->db->where("b.nasabah_id", $nasabah_id);
		$query = $this->db->get();

		return $query->row();
	}

	public function insert_nasabah($data)
	{
		$data['created_at'] = date('Y-m-d H:i:s');
		$result = $this->db->insert('nasabah', $data);

		if ($result == true) {
			return true;
		} else {
			return false;
		}
	}

	public function update_pinjaman($data, $pinjaman_id)
	{
		$this->db->where('pinjaman_id', $pinjaman_id);
		$result = $this->db->update('pinjaman', $data);
		
		return $data;;
	}

	public function update_nasabah($data, $nasabah_id)
	{
		$this->db->where('nasabah_id', $nasabah_id);
		$result = $this->db->update('nasabah', $data);
		
		return $data;;
	}

	public function update_user($data, $user_id){

		if($data['password']==''){
			unset($data['password']);
		}else{
			$data['password'] = md5($data['password']);
		}

		$this->db->where('user_id', $user_id);
		$result = $this->db->update('users', $data);

		if($result==true){
			return true;
		}else{
			return false;
		}
	}

	public function delete_user($user_id)
	{

		$this->db->where('user_id', $user_id);
		$this->db->delete('users');

		return true;
	}
	public function delete_nasabah($nasabah_id)
	{

		$this->db->where('nasabah_id', $nasabah_id);
		$this->db->delete('nasabah');

		return true;
	}
	public function delete_pinjaman($pinjaman_id)
	{

		$this->db->where('pinjaman_id', $pinjaman_id);
		$this->db->delete('pinjaman');
		
		return true;
	}
	public function delete_pengajuan($pengajuan_id)
	{

		$this->db->where('pengajuan_id', $pengajuan_id);
		$this->db->delete('pengajuan');
		
		return true;
	}
	
	public function reject_pengajuan($pengajuan_id)
	{

		$this->userdata->user_id;
		$data =  array(
			"status" => "reject",
			"updated_by" => $this->userdata->user_id,
			"updated_at" => date('Y-m-d H:i:s'),
		);

		$this->db->where('pengajuan_id', $pengajuan_id);
		$this->db->update('pengajuan', $data);
		
		return true;
	}
	
	public function approve_pengajuan($pengajuan_id)
	{
		$this->userdata->user_id;
		$data =  array(
			"status" => "approved",
			"updated_by" => $this->userdata->user_id,
			"updated_at" => date('Y-m-d H:i:s'),
		);

		$this->db->where('pengajuan_id', $pengajuan_id);
		$this->db->update('pengajuan', $data);
		
		return true;
	}
	
	public function update_karyawan($data)
	{
		$id_kar =  $data['nasabah_id'];
		$data2['status'] =  $data['status'];
		$data2['username'] =  $data['nik'];
		unset($data['nasabah_id']);
		$this->db->where('nasabah_id', $id_kar);
		$this->db->update('karyawan', $data);

		$this->db->where('nasabah_id', $id_kar);
		$this->db->update('users', $data2);
	}

	public function insert_karyawan($data)
	{

		$result = $this->db->insert('karyawan', $data);
		$insert_id = $this->db->insert_id();

		if ($result == true) {
			$datauser['nasabah_id'] = $insert_id;
			$datauser['username'] = $data['nik'];
			$datauser['nama_lengkap'] = $data['nama_lengkap'];
			$datauser['nasabah_id'] = $data['nasabah_id'];
			$datauser['password'] = md5($data['nik']);
			$datauser['email'] = $data['email'];
			$datauser['status'] = '1';

			$result = $this->db->insert('users', $datauser);

			$data2['nasabah_id'] = $insert_id;

			$result = $this->db->insert('saldo_cuti', $data2);

			return 'Karyawan Berhasil Di Masukkan';
		} else {
			return 'Karyawan Gagal Di Masukkan';
		}
	}

	public function insert_area_kerja($data)
	{
		$result = $this->db->insert('area_kerja', $data);
		if ($result == true) {
			return 'Area kerja Berhasil Di Masukkan';
		} else {
			return 'Area Kerja Gagal Di Masukkan';
		}
	}
	public function insert_kategori($data)
	{
		$result = $this->db->insert('category', $data);
		if ($result == true) {
			return 'Kategori Berhasil Di Masukkan';
		} else {
			return 'Kategori Gagal Di Masukkan';
		}
	}

	public function insert_divisi($data)
	{
		$result = $this->db->insert('divisi', $data);
		if ($result == true) {
			return 'Divisi Berhasil Di Masukkan';
		} else {
			return 'DIvisi Gagal Di Masukkan';
		}
	}

	public function delete_karyawan($id_kar)
	{
		$tkar = $this->get_karyawan_by_id($id_kar);
		//var_dump($tkar); die();	
		$this->db->where('nasabah_id', $tkar->nasabah_id);
		$result = $this->db->delete('saldo_cuti');



		$this->db->where('nasabah_id', $tkar->nasabah_id);
		$result = $this->db->delete('users');

		$this->db->where('nasabah_id', $tkar->nasabah_id);
		$result = $this->db->delete('karyawan');

		if ($result == true) {
			return 'Berhasil Di Delete';
		} else {
			return 'Gagal Di Delete';
		}
	}
	public function delete_area_kerja($id_area)
	{

		$this->db->where('id', $id_area);
		$this->db->delete('area_kerja');

		return true;
	}
	public function delete_jenis_cuti($id_jenis)
	{

		$this->db->where('id_jenis', $id_jenis);
		$this->db->delete('jenis_cuti');

		return true;
	}
	public function delete_divisi($id_divisi)
	{

		$this->db->where('id_divisi', $id_divisi);
		$this->db->delete('divisi');

		return true;
	}

	public function insert_user($data)
	{
		$result = $this->db->insert('users', $data);
		if ($result == true) {
			return 'User Berhasil Di Masukkan';
		} else {
			return 'User Gagal Di Masukkan';
		}
	}

	public function delete_cuti($id_cuti)
	{

		$this->db->where('id', $id_cuti);
		$this->db->delete('ketidakhadiran');

		return true;
	}

	public function delete_spv_therapist($id_spv)
	{

		$this->db->where('id_spv', $id_spv);
		$this->db->delete('spv_therapist');

		return true;
	}

	function isUserExist($username)
	{
		$this->db->select('user_id');
		$this->db->where('username', $username);
		$query = $this->db->get('users_acc');

		if ($query->num_rows() > 0) {
			return true;
		} else {
			return false;
		}
	}

	public function get_jenis_ketidakhairan($sub_cuti)
	{

		$this->db->select('*');
		$this->db->from('jenis');
		$this->db->where('category_id', $sub_cuti);
		$query = $this->db->get();

		return $query->result();
	}

	public function get_saldo_cuti($id)
	{
		$this->db->select('*');
		$this->db->from('saldo_cuti');
		$this->db->where('nasabah_id', $id);
		$query = $this->db->get();

		return $query->row('saldo');
	}

	public function get_jenis_cuti_all()
	{

		$this->db->select('*');
		$this->db->from('jenis');
		$query = $this->db->get();

		return $query->result();
	}
	public function get_category_all()
	{

		$this->db->select('*');
		$this->db->from('category');
		$query = $this->db->get();

		return $query->result();
	}

	public function get_divisi_all()
	{

		$this->db->select('*');
		$this->db->from('divisi');
		$query = $this->db->get();

		return $query->result();
	}
	public function get_area_kerja_all()
	{

		$this->db->select('*');
		$this->db->from('area_kerja');
		$query = $this->db->get();

		return $query->result();
	}

	public function get_category_name_by_cat_id($category_id)
	{

		$this->db->select('category_name');
		$this->db->from('category');
		$query = $this->db->get();

		return $query->row('category_name');
	}
	public function get_jabatan_name_by_id($id_jabatan)
	{

		$this->db->select('nama_jabatan');
		$this->db->where('id_jabatan', $id_jabatan);
		$this->db->from('jabatan');
		$query = $this->db->get();

		return $query->row('nama_jabatan');
	}
	public function get_area_kerja_by_id($area_kerja)
	{

		$this->db->select('lokasi');
		$this->db->where('id', $area_kerja);
		$this->db->from('area_kerja');
		$query = $this->db->get();

		return $query->row('lokasi');
	}
	public function get_divisi_name_by_id($id_divisi)
	{

		$this->db->select('nama_divisi');
		$this->db->where('id_divisi', $id_divisi);
		$this->db->from('divisi');
		$query = $this->db->get();

		return $query->row('nama_divisi');
	}
	public function get_jenis_cuti_name_by_id($kd_jenis)
	{

		$this->db->select('jenis_name');
		$this->db->where('id_jenis', $kd_jenis);
		$this->db->from('jenis_cuti');
		$query = $this->db->get();

		return $query->row('jenis_name');
	}
	public function get_category_name_by_id($kd_cat)
	{

		$this->db->select('category_name');
		$this->db->where('id_category', $kd_cat);
		$this->db->from('category');
		$query = $this->db->get();

		return $query->row('category_name');
	}

	public function get_karyawan_by_id($id_kar)
	{

		$this->db->select('karyawan.*');
		$this->db->select('jabatan.nama_jabatan');
		$this->db->select('divisi.nama_divisi');
		$this->db->from('karyawan');
		$this->db->join("jabatan", "jabatan.id_jabatan = karyawan.nasabah_id");
		$this->db->join("divisi", "divisi.id_divisi = karyawan.divisi_id");
		$this->db->where('karyawan.nasabah_id', $id_kar);
		$query = $this->db->get();
		//print_r($this->db->last_query()); die();
		return $query->row();
	}
}