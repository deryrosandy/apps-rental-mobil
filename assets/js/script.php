<script type="text/javascript">
	window.onload = function() {
		if ($(window).width() > 330) {
			$('.navbar').addClass('navbar-static-top');
		} else {
			$('.navbar').addClass('navbar-fixed-top');
		}

		$('#datatable1').DataTable({
			"scrollX": !0,
			"pageLength": 20,
			"info": true,
			"searching": false,
			"lengthMenu": [10, 20, 50, 75, 100],
			"columnDefs": [{
				"defaultContent": "-",
				"targets": "_all"
			}]
		});

		$(function() {
			$('.select2').select2();
		});

		$(function() {
			var date = new Date();
			var today = new Date(date.getFullYear(), date.getMonth(), date.getDate());
			$('.datetimepicker').datetimepicker({
				language: 'id',
				weekStart: 1,
				todayBtn: 1,
				autoclose: 1,
				todayHighlight: 1,
				startView: 2,
				minView: 2,
				forceParse: 0
			});
		});
		$(function() {
			var date = new Date();
			var today = new Date(date.getFullYear(), date.getMonth(), date.getDate());
			$('.print_date').datetimepicker({
				language: 'id',
				weekStart: 1,
				todayBtn: 1,
				autoclose: 1,
				todayHighlight: 1,
				startView: 2,
				minView: 2,
				forceParse: 0
			});
		});
	}

	var table;

	$(document).on('click', '.delete', function(){
		var id	 = $(this).data("id");
		var url = $(this).data("url");
		swal({
			title: "Apakah anda yakin ?",
			text: " ",
			type: "warning",
			showCancelButton: true,
			confirmButtonClass: "btn-danger",
			confirmButtonText: "Yes, delete it!",
			closeOnConfirm: false
			},
			function () {
				$.ajax({
					data: {"id" : id},
					url: url,
					type: "POST",
					success: function(msg){
						swal("Deleted!", "Delete Success", "success");		
						location.reload();					
					},
					error: function(){
						swal("Error", "Delete Failed", "error");
						location.reload();
					}					
				});  	               
			}
		);
	});

	$(document).on('click', '.approve', function(){
		var id	 = $(this).data("id");
		var url = $(this).data("url");
		swal({
			title: "Apakah anda yakin ?",
			text: " ",
			type: "info",
			showCancelButton: true,
			confirmButtonClass: "btn-primary",
			confirmButtonText: "Yes, Approve it!",
			closeOnConfirm: false
			},
			function () {
				$.ajax({
					data: {"id" : id},
					url: url,
					type: "POST",
					success: function(msg){
						swal("Approve!", "Approve Success", "success");		
						location.reload();					
					},
					error: function(){
						swal("Error", "Approve Failed", "error");
						location.reload();
					}					
				});  	               
			}
		);
	});

	$(document).on('click', '.reject', function(){
		var id	 = $(this).data("id");
		var url = $(this).data("url");
		swal({
			title: "Apakah anda yakin ?",
			text: " ",
			type: "warning",
			showCancelButton: true,
			confirmButtonClass: "btn-danger",
			confirmButtonText: "Yes, Reject it!",
			closeOnConfirm: false
			},
			function () {
				$.ajax({
					data: {"id" : id},
					url: url,
					type: "POST",
					success: function(msg){
						swal("Reject!", "Reject Success", "success");		
						location.reload();					
					},
					error: function(){
						swal("Error", "Reject Failed", "error");
						location.reload();
					}					
				});  	               
			}
		);
	});

	function formatCurrency(num) {

		//num = num.toString().replace(/\Rp|/g,'');
		if (isNaN(num))
			num = "0";
		sign = (num == (num = Math.abs(num)));
		num = Math.floor(num * 100 + 0.50000000001);
		cents = num % 100;
		num = Math.floor(num / 100).toString();
		if (cents < 10)
			cents = "0" + cents;
		for (var i = 0; i < Math.floor((num.length - (1 + i)) / 3); i++)
			num = num.substring(0, num.length - (4 * i + 3)) + '.' +
			num.substring(num.length - (4 * i + 3));

		return num;
	}

	function init_datatable() {
		$(document).ready(function() {
			//datatables
			table = $('#datatable').DataTable();
		});
	};

	function checkisNaN(num) {

		if (isNaN(num))
			num = 0;

		return num;
	}

	function jenis_kelamin(jeniskelamin) {
		if (jeniskelamin == 'L') {
			$(".jenis_kelamin").html("");
			var str2 = "<option selected value='L'>Laki-laki</option>" +
				"<option value='P'>Perempuan</option>";
			$(".jenis_kelamin").append(str2);
		} else if (jeniskelamin == 'P') {
			$(".jenis_kelamin").html("");
			var str2 = "<option selected value='L'>Laki-laki</option>" +
				"<option value='P'>Perempuan</option>";
			$(".jenis_kelamin").append(str2);
		} else {
			$(".jenis_kelamin").html("");
			var str2 = "<option>Pilih Jenis Kelamin</option>" +
				"<option value='L'>Laki-laki</option>" +
				"<option value='P'>Perempuan</option>";
			$(".jenis_kelamin").append(str2);
		}
	}
	

	var date = new Date();
	var end_date = new Date(date.getFullYear(), date.getMonth(), '20', 0);
	var start_date = new Date(date.getFullYear(), date.getMonth() - 1, '21', 0);
	var start_date_gro = new Date(date.getFullYear(), date.getMonth() - 1, '1', 0);
	var end_date_gro = new Date(date.getFullYear(), date.getMonth(), '0', 0);

	//Datepicker Dashboard Therapist
	$('#bulan_periode').datepicker({
		format: "yyyy-mm",
		autoclose: true,
		startView: "year",
		minViewMode: "months",
		todayBtn: "linked"
	}).datepicker();

	$('#bulan_periode').datepicker({
		//endDate: new Date,
		format: "yyyy-mm",
		autoclose: true,
		todayBtn: "linked",
		startView: "year",
		minViewMode: "months"
	}).datepicker("setDate", start_date);

	$('.datepicker').datepicker({
		format: "yyyy-mm-dd",
		autoclose: true,
		todayBtn: "linked"
	}).datepicker();

	//Date picker
	$('#tanggal_lahir').datepicker({
		//endDate: new Date,
		format: "yyyy-mm-dd",
		autoclose: true,
		todayBtn: "linked"
	}).datepicker();


	//Date picker
	$('#dari_tanggal').datepicker({
		//endDate: new Date,
		format: "yyyy-mm-dd",
		autoclose: true,
		todayBtn: "linked"
	}).datepicker("setDate", start_date);

	//console.log(start_date);
	//console.log(end_date);
	$('#sampai_tanggal').datepicker({
		//endDate: new Date,
		format: "yyyy-mm-dd",
		autoclose: true,
		todayBtn: "linked"
	}).datepicker("setDate", end_date);

	//Date picker
	$('#dari_tanggal_gro').datepicker({
		//endDate: new Date,
		format: "yyyy-mm-dd",
		autoclose: true,
		todayBtn: "linked"
	}).datepicker("setDate", start_date_gro);

	$('#sampai_tanggal_gro').datepicker({
		//endDate: new Date,
		format: "yyyy-mm-dd",
		autoclose: true,
		todayBtn: "linked"
	}).datepicker("setDate", end_date_gro);

	$(document).ready(function() {

		$(document).on("change", "#pilih_nasabah", function() {
			
			var nasabah_id = this.value;

			$.ajax({
				type: "POST",
				url: "<?php echo site_url('restrukturisasi/get_pinjaman_nasabah'); ?>",
				data:  {"nasabah_id": nasabah_id},
				dataType: "json",
				success: function(response) {
					$(".no_rek").val(response.no_rek);
					$(".jml_pinjaman").val(formatCurrency(response.jml_pinjaman));
					$(".tenor").val(response.tenor);
					$(".jml_cicilan_pokok").val(formatCurrency(response.cicilan_perbulan));
					//location.reload();
				},
				error: function() {
					alert("Gagal");
				}
			});

			// var kodekar = $('#kodekar').html();
			// var namakar = $('#namakar').html();
			// var dari_tanggal = $('#modal_komisi_lulur').attr('data-from');
			// var sampai_tanggal = $('#modal_komisi_lulur').attr('data-to');
		});

		function validasi_input(form){
			var mincar = 16;
			alert(form.noktp.value.length);
			if (form.noktp.value.length < mincar){
				alert("Panjang NIK KTP Minimal 16 Karater!");
				form.noktp.focus();
				return (false);
			}else{
				return (true);
			}
		}

	});

	function isChecked() {
		var isChecked = false;
		if (document.getElementById('checked_status').checked) {
			isChecked = true;
			console.log(isChecked);
		} else {
			isChecked = false;
			console.log(isChecked);
		}
	}

	function formatDate(date) {
		var monthNames = [
			"January", "February", "March",
			"April", "May", "June", "July",
			"August", "September", "October",
			"November", "December"
		];

		var day = date.getDate();
		var monthIndex = date.getMonth();
		var year = date.getFullYear();

		return day + ' ' + monthNames[monthIndex] + ' ' + year;
	}

	$('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
		checkboxClass: 'icheckbox_minimal-blue',
		radioClass: 'iradio_minimal-blue'
	})
</script>